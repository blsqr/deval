# The `deval` Package

The `deval` package is a Python package to be used for general evaluation purposes.

It is based on some very general classes that handle the reading, analysis, and plotting of data. Functionality specific to the type of data is implemented by inheriting and expanding these base classes.

## Current Functionality and Classes

* A `DataManager` class, that handles loading, evaluating, and plotting of the data.
* A data framework to read in data of different formats into one homogeneous, hierarchical data structure: `DataContainer` and `DataGroup` classes
* A plotting framework with helper functions and easy access to the data: `PlotHandler` class

(WIP)

# Installation

Required for `deval` is only Python3 and the packages named in `requirements.txt`.
Install both via:

```
$ brew install python3
$ pip3 install -r requirements.txt
```


# Usage

`deval` can currently be used either by (i) executing a previously configured script or, (ii), interactively in `IPython`.

## Usage with `deval.py`

The `deval.py` script handles the target (and probably most common) use case of the `deval` package:

1. Select a data directory
2. Load the data into a `DataManager`-derived class
3. Perform some pre-defined plots

For the loading of the data and the plots, a default configuration is supplied.
However, each of these steps can be further configured using a command line interface.
In the following, a few examples are given, but see the end of this file for the whole command line interface or call `$ python3 deval.py --help`.


### Default behaviour

To invoke it with defaults, use:

```
$ cd src/deval
$ python3 deval.py path/to/data/directory/
```

### Passing custom load configuration
To adjust the load configuration (which data is loaded and how), a path to a `*.yml` file can be supplied.
If the default configuration (loaded from `deval_cfg.yml`) is not sufficient for the desired evaluation, this feature becomes important.

```
$ python3 deval.py path/to/data/directory/ --load-cfg path/to/load_cfg.yml
```

A custom load configuration can, e.g., make use of further load methods or configure load methods such that they require a specific data shape. A load configuration takes the following form:

```yaml
# Load the general data under the `data` key
data:
  glob_str:             Data.h5   # How to find the data
  loader:               hdf5      # Name of the loading method
  required:             True      # True: raise if data could not be loaded
  # The following arguments are passed to the loader
  lower_case_keys:      True      # call .lower() on group & dset names

# Load the iterations data together into a group
iterations:
  glob_str:             "Iteration_*.h5" # may use wildcards here, creates sub-entries of the group
  exclude:              []        # May exclude specific filenames
  loader:               hdf5
  required:             False     # May be omitted, default is False
  always_create_group:  True      # May enforce creating the `iterations` group, even if the wildcard matches only one file.

config:
  glob_str:             "*.cfg"
  loader:               libconf   # Loads the libconf file
```

The data is then available to the `DataManager` under the keys `data`, `iterations`, and `config`.


#### Updating a load configuration

For minor changes to another load configuration, the `--update-load-cfg` argument can be used:

```
$ python3 deval.py path/to/data/directory/ --load-cfg path/to/load_cfg.yml --update-load-cfg path/to/load_update.yml
```

The file to update with can then simply be:

```yaml
iterations:
  required:             True
```


### Passing custom plot configuration
By default, a general set of plots will be configured in `default_plots.yml`. In many use cases, it is reasonable to create a custom plot configuration, which will match the specified data.

```
$ python3 deval.py path/to/data/directory/ --plot-cfg path/to/plot_cfg.yml
```

An example plot configuration may look like this:

```yaml
# A plot of all states
states:
  enabled:                True
  func_name:              states        # The function to use for plotting

  # Further arguments to the plotting function
  dimensions:             all           # 'all' or a sequence of dimension numbers
  iterations:             all           # 'all' or a sequence of iteration numbers

# A plot of a specific state parameter
state_parameter_0_0:
  enabled:                True
  func_name:              state_parameter

  iteration:              0
  dimension:              0

# A plot of all state parameters
state_parameters:
  enabled:                False         # Can be disabled
  func_name:              state_parameters

  iterations:             all
  dimensions:             all

# The hydrology plot
hydrology:
  enabled:                True
  func_name:              hydrology_over_time
  style:                  # Can adjust the style here
    title:                "State ({time:.0f}s, #{meas_no:}, {mode:})"
    limits:
      xlims:              [0., 0.44] # for consistent limits

  iterations:             all
  measurements:           all
  modes:                  [forecast, analysis] # order is preserved!
  min_depth:              0.
  max_depth:              1.9
```


#### Updating a plot configuration

Similar to how this is done with the load configuration, the plot configuration can also be updated: use the `--update-plot-cfg` argument and pass a `*yml` file.


#### Only plotting specific plots

Using the `--plot-only` argument, only specific plots can be performed, e.g., `--plot-only state_parameters hydrology` enables only the `state_parameter` and the `hydrology` plot and disables all others, regardless of their enabled/disabled state in the plot configuration.


## Interactive Usage

Initialise an `IPython` session inside the `src/deval` directory. In the following are a few examples of which commands are possible.

```python
import deval # will import everything necessary

# Create a data manager
dm = deval.DataManager(data_dir="path/to/knofu/data/directory/")

# Let it load the data using the default config
dm.load_data()

# ...or -- not yet documented feature -- with a given configuration as dict
# dm.load_data(load_cfg={'data': ...})
# see the `deval_cfg.yml` file, `default_load_cfg` key for examples of the structure. Documentation of this will follow; see the next section for some more information.

# Print the resulting data tree
print(dm)

# Access data according to the keys of the tree
dm['key1']['key2']['key3'].data  # access the data array directly
dm['foo']['bar'].attrs           # if from hdf5 file: dataset attributes
dm['bla'][23:42]                 # access contents of the (numeric) data array

# Read in a plotting configuration from a *.yml file
plt_cfg = deval.load_plt_config("path/to/plot_config_file.yml")
# ... or generate a dictionary yourself, see `default_plots.yml` for examples

# Perform these plots
dm.perform_plots(plt_cfg)
```

---

# `deval` Components and Extensions
## The `DataManager` Class

Currently implemented loaders are only `hdf5` and `libconf`.

Example configurations for the `hdf5` loader, to be used as an argument to the `DataManager.load_data()` method. Note that it is presented as `yaml` here, but the input needs to be a Python dictionary-like object with the following structure:

```yaml
data:                        # will be used as the root-level key for this data
  glob_str:        Data.h5   # which file to access, using `glob` internally
  loader:          hdf5      # the name of the loader
  required:        True      # whether this is required data, default: False

  # All following arguments are passed directly to the loader
  lower_case_keys: True      # .lower() on group & dset names, default: True
  # this is the only argument so far

  # This will create a `DataGroup` with the name `data`, and in it is -- hierarchically listed -- the content of the whole hdf5 file and its attributes.

iterations:
  glob_str:             "Iteration_*.h5"
  exclude:              []
  loader:               hdf5
  required:             True

  # Here, a more complex glob_str was used, which catches all data which match the given name. Additionally, some files matching this name can be excluded by specifiying them in the `exclude` list
  # This will create a `DataGroup` names `iterations`, and in it will be `DataGroups` for each file matching the `glob_str` argument. Note that if the `glob_str` only matches one file, the outer group will *not* be created. To enfore that, add the `always_create_group: True` entry.
```

### Extending the `DataManager`

With a general loader function: Add it to `DataManager`.

With a specific loader function: Inherit from `DataManager` and add it as a class method with the prefix `load_`.


## The `PlotHandler`

### Extending the `PlotHandler`


---
# Full Command Line Interface for `deval.py`

```
usage: deval.py [-o [OUT_DIR]]
                 [-l [LOAD_CFG]]
                 [--update-load-cfg [UPDATE_LOAD_CFG]]
                 [-p [PLOT_CFG]]
                 [--update-plot-cfg [UPDATE_PLOT_CFG]]
                 [--plot-only [PLOT_ONLY [PLOT_ONLY ...]]]
                 [--raise]
                 [--hide-tree]
                 [-h]
                 data_dir

Use deval to perform evaluation and plots.

positional arguments:
  data_dir              The data directory to be loaded and plotted.

optional arguments:
  -h, --help            show this help message and exit
  -o [OUT_DIR], --out-dir [OUT_DIR]
                        Where the output directory should be created. by
                        default, it is created inside the data directory.
  -l [LOAD_CFG], --load-cfg [LOAD_CFG]
                        From which *.yml file to read the load configuration,
                        i.e., which data should be imported and how. If none
                        is given, the default load configuration of the
                        DataManager will be used.
  --update-load-cfg [UPDATE_LOAD_CFG]
                        If supplied, the content of this *.yml file will be
                        used to recursively update the previously determined
                        load configuration during (!) the .load_data() call.
                        This can be used to extend the load configuration or
                        make minor changes, while baseing this on a given file
                        or the defaults.
  -p [PLOT_CFG], --plot-cfg [PLOT_CFG]
                        From which *.yml file to read the list of plots to
                        perform. If none is given, the default plot
                        configuration will be used: default_plots.yml
  --update-plot-cfg [UPDATE_PLOT_CFG]
                        If supplied, the content of this *.yml file will be
                        used to recursively update the previously determined
                        plot configuration. This can be used to extend the
                        plot configuration or make minor changes, while
                        baseing this on another file or the defaults.
  --plot-only [PLOT_ONLY [PLOT_ONLY ...]]
                        The plot names specified here will be the only ones
                        plotted, even if they are disabled in the plot
                        configuration. All other plots will be disabled.
  --raise, --debug      In debug mode, more (but not all) caught exceptions
                        are raised instead of just logging an error. Use this
                        to get a traceback to the error.
  --hide-tree           Suppresses the logging of the data tree.

Happy evaluations! :)
```
