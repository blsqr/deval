#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' This sets up everything for a general deval execution.'''

import os
import shutil
import argparse

import deval
from deval.tools import tty_centred_msg

# Setup logging
log	= deval.get_logger(__name__)
log.debug("Logger loaded.")

# Local constants
# Default plot configuration, relative to deval/__init__.py
PLOT_CFG 	= '../default_plots.yml'
# NOTE Depending on where deval.py is executed, this might not be the desired path; therefore it is made absolute with respect to the package's directory to account for this.

# Minor actions
# Transform the default plot configuration to an absolute path
PLOT_CFG 	= os.path.abspath(os.path.join(deval.PKG_DIR, PLOT_CFG))


# Main code
if __name__ == '__main__':
	log.debug("Setting up ArgumentParser ...")

	# Setup argument parser and command line arguments
	parser = argparse.ArgumentParser(description="Use deval to perform evaluation and plots.", epilog="Happy evaluations! :)")
	parser.add_argument('data_dir',
	                    action='store',
	                    help="The data directory to be loaded and plotted.")
	parser.add_argument('-o', '--out-dir',
	                    nargs='?',
	                    dest='out_dir',
	                    default=None,
	                    help="Where the output directory should be created. By default, it is created in a sub-directory inside the data directory.")
	parser.add_argument('-p', '--plot-cfg',
	                    nargs='?',
	                    dest='plot_cfg',
	                    default=None,
	                    help="From which *.yml file to read the list of plots to perform. If none is given, the default plot configuration will be used: " + PLOT_CFG)
	parser.add_argument('--update-plot-cfg',
	                    nargs='?',
	                    dest='update_plot_cfg',
	                    default=None,
	                    help="If supplied, the content of this *.yml file will be used to recursively update the previously determined plot configuration. This can be used to extend the plot configuration or make minor changes, while baseing this on another file or the defaults.")
	parser.add_argument('--plot-only',
	                    nargs='*',
	                    dest='plot_only',
	                    default=None,
	                    help="The plot names specified here will be the only ones plotted, even if they are disabled in the plot configuration. All other plots will be disabled.")
	parser.add_argument('-l', '--load-cfg',
	                    nargs='?',
	                    dest='load_cfg',
	                    default=None,
	                    help="From which *.yml file to read the load configuration, i.e., which data should be imported and how. If none is given, the default load configuration of the DataManager will be used.")
	parser.add_argument('--update-load-cfg',
	                    nargs='?',
	                    dest='update_load_cfg',
	                    default=None,
	                    help="If supplied, the content of this *.yml file will be used to recursively update the previously determined load configuration during (!) the .load_data() call. This can be used to extend the load configuration or make minor changes, while baseing this on a given file or the defaults.")
	parser.add_argument('--raise', '--debug',
	                    action='store_true',
	                    dest='debug',
	                    default=False,
	                    help="In debug mode, more (but not all) caught exceptions are raised instead of just logging an error. Use this to get a traceback to the error.")
	parser.add_argument('--hide-tree',
	                    action='store_true',
	                    default=False,
	                    help="Suppresses the logging of the data tree. This flag should be supplied if a lot of data is imported and the tree output would slow the script execution down.")

	# Parse these arguments
	args = parser.parse_args()
	log.debug("Command line arguments parsed. Got:\n%s", deval.PP(vars(args)))


	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Before doing anything else, evaluate the arguments that change package-wide variables

	if args.debug:
		log.note("Setting the package-wide DEBUG flag...")
		deval.DEBUG 	= True
		log.debug("DEBUG: %s", deval.DEBUG)


	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Now get to the loading of configurations and creation of the DataManager
	# Determine the load dictionary, if a path was passed
	if args.load_cfg:
		log.note("Loading custom load configuration ...")
		log.state("...from:  %s", args.load_cfg)
		load_cfg 	= deval.load_cfg_file(args.load_cfg)
	else:
		load_cfg 	= None


	# Determine the plot dictionary by loading the specified config file or the default configuration (if no config file was specified)
	if args.plot_cfg:
		log.note("Loading custom plot configuration ...")
		_plot_cfg 	= args.plot_cfg
	else:
		log.note("Loading default plot configuration ...")
		_plot_cfg 	= PLOT_CFG

	log.state("...from:  %s", _plot_cfg)

	plot_cfg 	= deval.load_plt_config(_plot_cfg, plot_only=args.plot_only)


	# The update dicts for both the load and the plot configs
	update_load_cfg = None
	update_plot_cfg = None

	if args.update_load_cfg:
		log.note("Loading config file to update load configuration ...")
		update_load_cfg 	= deval.load_cfg_file(args.update_load_cfg)

	if args.update_plot_cfg:
		log.note("Loading config file to update plot configuration ...")
		update_plot_cfg 	= deval.load_cfg_file(args.update_plot_cfg)

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Initialise DataManager object from the given data directory
	dm	= deval.DataManager(data_dir=args.data_dir,
	                        out_dir=args.out_dir,
	                        load_cfg=load_cfg)

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Copy the referenced yml files to the output directory
	log.progress("Copying provided config files to output directory ...")
	dst_names = ['load_cfg', 'update_load_cfg', 'plot_cfg', 'update_plot_cfg']
	src_files = [getattr(args, dn, None) for dn in dst_names]

	# Add the default plot config manually
	src_files.append(_plot_cfg)
	dst_names.append('default_plt_cfg')

	for src, dst_name in zip(src_files, dst_names):
		if not src:
			log.debugv("No %s given to copy.", dst_name)
			continue

		# Create destination path and create the directory
		dst 	= os.path.join(dm.dirs['cfg'], dst_name+".yml")
		os.makedirs(os.path.dirname(dst), exist_ok=True)

		# Perform the copying.
		shutil.copy2(src, dst)
		# NOTE using copy2 as it preserves metadata and permissions
		log.debug("Copyied %s to output directory ...", dst_name)
		log.debugv("Full path:\n  %s", dst)

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Load the data and perform the plots.
	log.highlight("\n\n%s", tty_centred_msg(" Loading data ... "))

	# Call the load method. This will use the load config passed at initialisation or, if none was passed, the DataManager defaults
	dm.load_data(update_load_cfg=update_load_cfg,
	             log_data=(not args.hide_tree))
	# Data is now available in the DataManager object


	log.highlight("\n\n%s", tty_centred_msg(" Performing plots ... "))

	# Call the plotting method with the desired plot configuration.
	dm.perform_plots(plot_cfg=plot_cfg, update_plot_cfg=update_plot_cfg)

	# Done.
	log.highlight("\n\n%s\n\n", tty_centred_msg(" All done. "))
