#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' The deval package is a package that can be used as a general data evaluation suite.'''

import os
import logging

import deval.tools
from deval.config import get_config, setup_logger

# Version information
__version__ = "0.3.7"

# Setup logging
# The logging method is made available under a custom name that is imported by the other deval modules
setup_logger()
get_logger 	= logging.getLogger
log			= get_logger(__name__)

# Start initialising the rest now ...
log.highlight("Initialising deval package ...")

# Load the configuration
cfg 		= get_config(__name__)

# Set the matplotlib backend such that it does not cause problems elsewhere
import matplotlib
matplotlib.use(cfg.pkg.matplotlib.backend)

# Make formatters available
tree_printer= deval.tools.TreePrinter(indent=2, sort_keys=True)
PP 			= deval.tools.pprint

# The package-wide debug flag
DEBUG 		= False

# Some other package-wide variables
PKG_DIR 	= os.path.dirname(__file__)


# Import from other modules such that one can do `import deval` somewhere and be able to access the most important methods.
log.debug("Supplying basic classes and methods ...")

from deval.config import load_plt_config, load_cfg_file
import deval.exceptions as exc

import deval.plot_wrapr as wrappers
from deval.data_cont import DataContainer, DataGroup, NumpyDC
from deval.data_mngr import DataManager
from deval.plot_hdlr import PlotHandler
