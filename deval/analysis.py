#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' This module is a toolbox for different analysis methods working on previously imported data.'''

import numpy as np

import deval
from deval import PP

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------
