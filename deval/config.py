''' For setting up all the general configuration needed in the deval package, e.g. config file and logging config

NOTE: there is no config variable available in this module.
'''

import os
import re
import collections
import warnings
import logging
import logging.config
import yaml
from pkg_resources import resource_filename

import numpy as np

from easydict import EasyDict

# NOTE to avoid circular imports, this file may not access any other parts of the deval package.

# Setup logging for this file
log	= logging.getLogger(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEFAULT_CFG 	= resource_filename('deval', 'deval_cfg.yml')
DEFAULT_LOG_CFG = resource_filename('deval', 'log_cfg.yml')
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Logger
def setup_logger(cfg_path: str=DEFAULT_LOG_CFG):
	''' Sets up the logging module'''
	try:
		with open(cfg_path, "r") as fd:
			# Get the configuration dictionary
			log_cfg = yaml.safe_load(fd.read())

	except FileNotFoundError as err:
		raise FileNotFoundError("Could not find {}".format(cfg_path)) from err

	else:
		# Add additional log levels
		_add_additional_log_levels()

		# Configure the logger from the dictionary
		logging.config.dictConfig(log_cfg['main'])

		root 	= logging.getLogger()

		# Try to set a colored logger
		try:
			from colorlog import ColoredFormatter

		except ImportError:
			# module not available
			pass

		else:
			# Configure colored logging
			clog_cfg 			= log_cfg['coloredFormatter']
			coloredFormatter 	= ColoredFormatter(clog_cfg['formatstr'],
			                                       **clog_cfg['kwargs'])

			# Try to set the formatter of the Stream Handlers
			for handler in root.handlers:
				if handler._name == 'consoleHandler':
					handler.setFormatter(coloredFormatter)
					root.debug("Changed formatter of StreamHandler to ColoredFormatter.")

		root.debug("Logger initialised.")

def _add_additional_log_levels():
	'''Adds additional log levels to allow more granular logging.'''

	# Add additional log level for repetitively called log messages
	DEBUG_LEVELV_NUM = 5
	logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
	def debugv(self, message, *args, **kws):
		if self.isEnabledFor(DEBUG_LEVELV_NUM):
			# prefix "|- " for this level, because it is often repetitive stuff
			self._log(DEBUG_LEVELV_NUM, "|- " + message, args, **kws)
	logging.Logger.debugv = debugv

	# Add additional log level for info on progress
	PROGRSS_LEVEL_NUM = 13 	#  (between DEBUG and INFO)
	logging.addLevelName(PROGRSS_LEVEL_NUM, "PROGRSS")
	def progress(self, message, *args, **kws):
		if self.isEnabledFor(PROGRSS_LEVEL_NUM):
			self._log(PROGRSS_LEVEL_NUM, message, args, **kws)
	logging.Logger.progress = progress

	# Add additional log level for messages on system state
	STATE_LEVEL_NUM = 15 	#  (between DEBUG and INFO)
	logging.addLevelName(STATE_LEVEL_NUM, "STATE")
	def state(self, message, *args, **kws):
		if self.isEnabledFor(STATE_LEVEL_NUM):
			self._log(STATE_LEVEL_NUM, message, args, **kws)
	logging.Logger.state = state

	# Add additional log level for messages that are a bit less important than information messages
	NOTE_LEVEL_NUM = 17 	#  (between STATE and INFO)
	logging.addLevelName(NOTE_LEVEL_NUM, "NOTE")
	def note(self, message, *args, **kws):
		if self.isEnabledFor(NOTE_LEVEL_NUM):
			self._log(NOTE_LEVEL_NUM, message, args, **kws)
	logging.Logger.note = note

	# Add additional log level for highlighted messages (that are not warnings)
	HIGHLIGHT_LEVEL_NUM = 25 	#  (between STATE and INFO)
	logging.addLevelName(HIGHLIGHT_LEVEL_NUM, "HILIGHT")
	def highlight(self, message, *args, **kws):
		if self.isEnabledFor(HIGHLIGHT_LEVEL_NUM):
			self._log(HIGHLIGHT_LEVEL_NUM, message, args, **kws)
	logging.Logger.highlight = highlight

	# Add additional log level for warning messages that are just slightly below warnings
	CAUTION_LEVEL_NUM = 29 	#  (between STATE and INFO)
	logging.addLevelName(CAUTION_LEVEL_NUM, "CAUTION")
	def caution(self, message, *args, **kws):
		if self.isEnabledFor(CAUTION_LEVEL_NUM):
			self._log(CAUTION_LEVEL_NUM, message, args, **kws)
	logging.Logger.caution = caution

setup_logger()
# -----------------------------------------------------------------------------
# yaml stuff

# Add custom YAML-constructors
# ...for simple math expressions
def _expr_constructor(loader, node):
	''' pyyaml constructor for evaluating strings with simple mathematical expressions.

	Supports: +, -, *, /, e-X, eX
	'''

	log.debug("Encountered !expr tag. Constructing scalar ...")

	# get expression string
	expr_str 	= loader.construct_scalar(node)

	log.debugv("expr_str:  %s", expr_str)

	# Remove spaces
	expr_str 	= expr_str.replace(" ", "")

	# Parse some special strings
	if expr_str in ['np.nan', 'nan', 'NaN']:
		log.debugv("Returning np.nan")
		return np.nan

	elif expr_str in ['np.inf', 'inf', 'INF']:
		log.debugv("Returning np.inf")
		return np.inf

	elif expr_str in ['-np.inf', '-inf', '-INF']:
		log.debugv("Returning -np.inf")
		return -np.inf

	# remove everything that might cause trouble -- only allow digits, dot, +, -, *, /, and eE to allow for writing exponentials
	expr_str 	= re.sub(r'[^0-9eE\-.+\*\/]', '', expr_str)

	# Try to eval
	log.debugv("Returning eval(%s)", expr_str)
	return eval(expr_str)
yaml.add_constructor(u'!expr', _expr_constructor)

# ...for constructing slice objects
def _slice_constructor(loader, node):
	''' pyyaml constructor for slices'''

	log.debug("Encountered !slice tag.")

	# get slice arguments either from a scaler or from a sequence
	if isinstance(node, yaml.nodes.SequenceNode):
		slc_args 	= loader.construct_sequence(node, deep=True)
	else:
		# Assuming scalar, loading into list to be unpackable
		slc_args 	= [loader.construct_scalar(node)]

	log.debugv("slc_args:  %s", slc_args)

	slc 	= slice(*slc_args)

	log.debugv("Created slice: %s", slc)

	return slc
yaml.add_constructor(u'!slice', _slice_constructor)

def _range_constructor(loader, node):
	''' pyyaml constructor for range'''
	log.debug("Encountered !range tag.")

	# get range arguments either from a scaler or from a sequence
	if isinstance(node, yaml.nodes.SequenceNode):
		rg_args 	= loader.construct_sequence(node, deep=True)
	else:
		# Assuming scalar, loading into list to be unpackable
		rg_args 	= [loader.construct_scalar(node)]

	log.debugv("rg_args:  %s", rg_args)

	rg 	= range(*rg_args)

	log.debugv("Created range: %s", rg)

	return rg
yaml.add_constructor(u'!range', _range_constructor)

# -----------------------------------------------------------------------------
# Loading config files

def get_config(modstr: str, cfg_fpath: str=DEFAULT_CFG, return_leaf: bool=True, ConfigClass=EasyDict, base_module: str='deval', add_constructors=None) -> dict:
	'''Loads a yaml-formatted configuration file ... [parse] ... and returns the content as a dict-like object.

	Procedure:
		load the

	Args:
		modstr (str) : dot-separated module string for the current module for which the config should be loaded

		TODO finish writing docstring

	Returns:
		config (ConfigClass) : the parsed config file as an ConfigClass object, where it is possible to access keys like attributes

	'''

	log.debug("Loading config file for module '%s' ...", modstr)

	cfg_dict 	= load_cfg_file(cfg_fpath, add_constructors=add_constructors)

	if not cfg_dict:
		log.error("Empty config dictionary.")
		return {}

	if return_leaf:
		log.debug("Traversing to leaf of config tree '%s' ...", modstr)

		modlst 	= modstr.split(".")

		if len(modlst) > 1:
			# There is a leaf to traverse to

			if base_module and base_module == modlst[0]:
				# Ignore the base module
				modlst 	= modlst[1:]

			try:
				cfg_dict 	= traverse_dict(cfg_dict, *modlst)

			except KeyError:
				log.warning("Entry for module %s not found; returning empty dict for this module.\nCheck your configuration file: %s", modstr, cfg_fpath)
				cfg_dict 	= {}

			else:
				# Successfully traversed.
				log.debugv("Traversed to leaf of module config '%s'.", modstr)

		else:
			# No leaf to traverse to, just return the current config dict
			pass

	else:
		log.debug("Returning config for module '%s'.", modstr)

	return ConfigClass(cfg_dict)

def load_cfg_file(fpath: str, add_constructors=None) -> dict:
	''' Loads and returns the config file at path fpath. It is intentional, that it will raise an error if the file is not present.

	# TODO reimplement safe_load http://pyyaml.org/wiki/PyYAMLDocumentation
	'''
	log.debug("Loading config file from '%s' ...", fpath)

	# Warn, if the file is not present
	if not os.path.isfile(fpath):
		raise FileNotFoundError(fpath)

	# Add additional constructors to yaml
	if add_constructors:
		# Loop over the given constructors
		for constr in add_constructors:
			# Extract the tag from the function attribute
			tag	= constr.tag
			# Create the constructor
			yaml.add_constructor(tag, constr)

	with open(fpath, 'r', encoding='utf-8') as f:
		try:
			cfg_ = yaml.load(f.read())
		except TypeError as err:
			log.error("Could not load config file from %s, probably because it is empty.\n", fpath, exc_info=True)
			raise

	log.debug("Successfully loaded config file.")
	return dict(cfg_)

# -----------------------------------------------------------------------------
# Load configuration files for plotting

def load_plt_config(path: str, plot_only: list=None, ConfigClass=EasyDict) -> dict:
	'''Loads a config file that is to be used for determining which plots will be performed.

	If the `plot_only` argument is given, the plots that are listed there will be enabled while all others will be disabled.

	Returns:
		ConfigClass: plot configuration dictionary. Everything below the 'plots' key.
	'''
	cfg 	= load_cfg_file(path)

	plt_cfg = cfg.get('plots')

	if not plt_cfg:
		log.warning("No plots configured in file '%s' under key 'plots'! Make sure that the plots are gathered under that key -- this has the advantage of allowing non-interfering variable definitions aside the level of the 'plots' key...", path)
		return {}

	# See if the plots list has to be reduced and adjusted
	if plot_only:
		log.note("Got `plot_only` argument. Enabling the following %d plot(s); disabling all others ...\n  %s", len(plot_only), ", ".join(plot_only))
		# Go over the list of keys and manually enable all that are in the plot_only list; disable all others
		for key in plt_cfg.keys():
			plt_cfg[key]['enabled']	= bool(key in plot_only)

	return ConfigClass(plt_cfg)

# -----------------------------------------------------------------------------
# Helpers

def traverse_dict(d, *keys):
	''' Recursively goes through a dictionary d and traverses along the given keys. Fails, if the key is not present.'''
	if len(keys) > 1:
		return traverse_dict(d[keys[0]], *keys[1:])
	elif len(keys) == 1:
		return d[keys[0]]
	else:
		# no keys passed - have this case for generality
		return d
