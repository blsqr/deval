#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' The data container module implements the DataContainer class, which is a
hierarchically organised container for all kinds of data. It works best in
combination with the DataManager class, which can be seen as the top-level.'''

import os
import re
from collections import OrderedDict

import numpy as np
import h5py as h5
from easydict import EasyDict

import deval
import deval.tools

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class DataContainerBaseClass:

	def __init__(self, *, name: str, data):

		log.debug("DataContainerBaseClass.__init__ called from %s '%s' ...", self.classname, name)

		self._name 		= name # always read-only
		self._data 		= data # can be read-only, if there is no data.setter

		log.debug("DataContainerBaseClass.__init__ finished.")

	# .........................................................................
	# Properties

	@property
	def name(self):
		return self._name

	@property
	def classname(self):
		'''Returns the name of this class'''
		return self.__class__.__name__

	@property
	def data(self):
		# Have to check whether the data might be a proxy. If so, resolve it.
		if self.data_is_proxy:
			log.debug("Resolving %s for %s '%s' ...", self._data.__class__.__name__, self.classname, self.name)
			self._data 	= self._data.resolve()

		# Now, the data should be loaded
		return self._data

	@data.setter
	def data(self, val):
		# Have to check whether the data might be a proxy. If so, resolve it.
		if self.data_is_proxy:
			log.debug("Resolving %s for %s '%s' ...", self._data.__class__.__name__, self.classname, self.name)
			self._data 	= self._data.resolve()

		# Now can set the value
		self._data 	= val

	@property
	def data_is_proxy(self):
		'''Returns true, if this is proxy data'''
		return isinstance(self._data, DataProxy)

	@property
	def proxy_data(self):
		'''If the data is proxy, returns the proxy data without using the .data attribute (which would trigger resolving the proxy); else returns None.'''
		if self.data_is_proxy:
			return self._data
		else:
			return None

	# .........................................................................
	# Special Methods

	def __getitem__(self, key):
		'''Returns an entry from the data container.'''
		if isinstance(key, list):
			# Convert Nones to None-slices
			key	= tuple([key if key is not None else slice(None)
			            for key in key])

		return self.data[key]

	def __setitem__(self, key, val):
		'''Sets an entry of the data container.'''
		self.data[key] 	= val

	def __contains__(self, val):
		'''Pass on to the data.'''
		return bool(val in self.data)

	def __len__(self):
		'''Returns the length of the data.'''
		return len(self.data)

	# .........................................................................
	# Formatting

	def __format__(self, spec_str: str):
		specs 		= spec_str.split(",")
		parts 		= []
		join_char 	= ", "

		for spec in specs:
			if spec in ['cls_name']:
				parts.append(self.classname)

			elif spec in ['info']:
				if not self.data_is_proxy:
					info_str 	= self.info_str
				else:
					info_str 	= self.proxy_data.info_str
				parts.append(info_str)

			else:
				raise ValueError("Invalid format string spec '{}' for {}.".format(spec, self.classname))

		return join_char.join(parts)

	@property
	def info_str(self):
		'''An info string, that describes the object. Each class should implement this to return an informative response.'''
		return "{:d} entries".format(len(self))

	# .........................................................................
	# Generators

	def keys(self):
		'''Returns an iterator over the keys of the data.'''
		return self.data.keys()

	def values(self):
		'''Returns an iterator over the values of the data.'''
		return self.data.values()

	def items(self):
		'''Returns an iterator over the (keys, values) tuple of the data.'''
		return self.data.items()

# -----------------------------------------------------------------------------

class DataAttributes(DataContainerBaseClass):
	'''A DataAttributes object manages attributes that are part of DataContainer objects. This class derives from the base class as otherwise there would be circular derivations.'''

	def __init__(self, attrs: dict=None, **dc_kwargs):

		if attrs is None:
			attrs 	= {}

		elif isinstance(attrs, h5.AttributeManager):
			# Manually read in the data
			attrs 	= {k: v for k, v in attrs.items()}
			# TODO might need a copy or import method here

		super().__init__(name="attrs", data=attrs, **dc_kwargs)

		log.debug("DataAttributes.__init__ finished.")

# -----------------------------------------------------------------------------

class DataContainer(DataContainerBaseClass):
	'''The DataContainer extends the base class by its ability to holds attributes.'''

	def __init__(self, *, name: str, data, attrs=None):

		log.debug("Initialising %s '%s' ...", self.classname, name)

		super().__init__(name=name, data=data)

		# Additionally to name and data, add the attributes object.
		self._attrs 	= DataAttributes(attrs)

		log.debug("DataContainer.__init__ finished.")

	@property
	def attrs(self):
		return self._attrs

	def convert_to(self, TargetCls, **target_init_kwargs):
		'''Creates a new TargetCls object with a (shallow) copy of the current container's properties.'''

		log.debug("Converting %s '%s' to %s ...", self.classname, self.name, TargetCls.__name__)

		# NOTE: Only name, data, and attrs are carried over!
		return TargetCls(name=self.name, data=self.data,
		                 attrs=self.attrs, **target_init_kwargs)

# -----------------------------------------------------------------------------

class BaseDataGroup(DataContainer):
	pass

class DataGroup(BaseDataGroup):
	'''A DataGroup is a DataContainer that gathers DataContainers. It is the
	element that creates the hierarchical ordering.

	Internally, the data attribute is an OrderedDict, that stores references to further DataContainer-derived classes (including DataGroups).

	The group can be initialised in three ways:
	1) With a list of the container-like objects it should include, where the names of the objects are used as keys. (Suggested way.)
	2) By passing a dict-like `data` argument, which is just converted into the OrderedDict. This only works, if no `containers` argument was passed
	3) Without passing anything; an empty group is created.
	'''

	def __init__(self, *, name: str, containers: list=None, data: dict=None, **dc_kwargs):

		log.debug("Initialising %s '%s' ...", self.classname, name)

		if containers is not None:
			# Read the container names and generate an ordered dict from it.

			if not all([isinstance(c, DataContainer) for c in containers]):
				raise ValueError("The argument containers can only have DataContainer-derived objects as contents.")

			# Build an OrderedDict with the container names
			names 	= [c.name for c in containers]
			data 	= OrderedDict()

			for name, container in zip(names, containers):
				data[name] 	= container

		elif data is not None:
			# Just convert into an OrderedDict
			data 	= OrderedDict(data)

		else:
			# Use an empty dict
			data 	= OrderedDict()

		# Initialise with parent method
		super().__init__(name=name, data=data, **dc_kwargs)

		# Done.
		log.debug("DataGroup.__init__ finished.")

	# .........................................................................
	# Special Methods

	def __getitem__(self, key):
		'''Like the regular getitem, but allows passing of a list of keys, which is then traversed.'''
		if isinstance(key, list):
			if len(key) > 1:
				# recursive step
				return super().__getitem__(key[0])[key[1:]]
			else:
				# reached the end of recursion
				key 	= key[0]
				# normal getitem does the rest ...

		return super().__getitem__(key)

	def __setitem__(self, key, val):
		'''Like setitem, but allows passing a list of keys to traverse through and set the value of the leaf.'''
		if isinstance(key, list):
			if len(key) > 1:
				raise NotImplementedError("Passing a sequence of keys for setting an item is not yet implemented.")
			else:
				key 	= key[0]

		super().__setitem__(key, val)

	def __str__(self):
		'''Generates a tree diagram of the keys of each entry in this group.'''
		# Get the tree and format it using the module's PP class
		return deval.tree_printer(self.return_tree(), key_length=34,
		                          obj_fstr="<{obj:cls_name,info}>", )

	# .........................................................................
	# Loading into the group

	def rec_update(self, dg):
		'''Recursively updates the contents of this DataGroup with the entries from the given DataGroup-like object'''

		# Loop over the given DataGroup
		for k, v in dg.items():
			# Distinguish between the case where it is another DataGroup and where it is
			if isinstance(v, BaseDataGroup):
				# Already a DataGroup -> continue recursion, if a group with the same name is already present; if not, just create an entry and save it there
				if k in self:
					# Continue recursion
					self[k].rec_update(v)
				else:
					self[k] = v

			else:
				# Not a DataGroup; add it to this DataGroup under the key
				self[k] = v

		log.debug("Finished recursive update of %s '%s'.", self.classname, self.name)

	# .........................................................................

	def return_tree(self) -> dict:
		'''Returns a nested dict which representes the tree of entries below this group.'''

		tree = {}
		for key, obj in self.items():
			if hasattr(obj, 'return_tree'):
				# Travel down ...
				tree[key] 	= obj.return_tree()
			else:
				# Reached leaf
				tree[key] 	= obj

		return tree

# -----------------------------------------------------------------------------

class NumpyDC(DataContainer):
	'''A DataContainer that works explicitly with Numpy arrays as data. It carries over some convenience functions to access the underlying data directly.

	The underlying numpy array can be accessed via the data attribute.
	'''

	# TODO check for correct initialisation?

	# .........................................................................
	# Formatting

	@property
	def info_str(self) -> str:
		'''Inform about shape and dtype'''
		return "{shape:}, {dtype:}".format(shape=self.shape,
			                               dtype=self.dtype)

	# .........................................................................
	# Special methods

	# .........................................................................
	# Numpy related properties

	@property
	def shape(self):
		return self.data.shape

	@property
	def dtype(self):
		return self.data.dtype

	# .........................................................................
	# Numpy related other methods

	def copy(self, **kwargs):
		'''Returns a copy of the data array.'''
		return self.data.copy(**kwargs)

	# .........................................................................
	# Comparisons and caluclations -- forwarding to data attribute

	def __add__(self, other):
		'''Forwards to the __add__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__add__(other)
	def __sub__(self, other):
		'''Forwards to the __sub__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__sub__(other)
	def __mul__(self, other):
		'''Forwards to the __mul__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__mul__(other)
	def __truediv__(self, other):
		'''Forwards to the __truediv__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__truediv__(other)
	def __floordiv__(self, other):
		'''Forwards to the __floordiv__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__floordiv__(other)
	def __mod__(self, other):
		'''Forwards to the __mod__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__mod__(other)
	def __divmod__(self, other):
		'''Forwards to the __divmod__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__divmod__(other)
	def __pow__(self, other):
		'''Forwards to the __pow__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__pow__(other)
	def __lshift__(self, other):
		'''Forwards to the __lshift__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__lshift__(other)
	def __rshift__(self, other):
		'''Forwards to the __rshift__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rshift__(other)
	def __and__(self, other):
		'''Forwards to the __and__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__and__(other)
	def __xor__(self, other):
		'''Forwards to the __xor__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__xor__(other)
	def __or__(self, other):
		'''Forwards to the __or__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__or__(other)
	def __radd__(self, other):
		'''Forwards to the __radd__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__radd__(other)
	def __rsub__(self, other):
		'''Forwards to the __rsub__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rsub__(other)
	def __rmul__(self, other):
		'''Forwards to the __rmul__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rmul__(other)
	def __rtruediv__(self, other):
		'''Forwards to the __rtruediv__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rtruediv__(other)
	def __rfloordiv__(self, other):
		'''Forwards to the __rfloordiv__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rfloordiv__(other)
	def __rmod__(self, other):
		'''Forwards to the __rmod__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rmod__(other)
	def __rdivmod__(self, other):
		'''Forwards to the __rdivmod__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rdivmod__(other)
	def __rpow__(self, other):
		'''Forwards to the __rpow__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rpow__(other)
	def __rlshift__(self, other):
		'''Forwards to the __rlshift__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rlshift__(other)
	def __rrshift__(self, other):
		'''Forwards to the __rrshift__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rrshift__(other)
	def __rand__(self, other):
		'''Forwards to the __rand__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rand__(other)
	def __rxor__(self, other):
		'''Forwards to the __rxor__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__rxor__(other)
	def __ror__(self, other):
		'''Forwards to the __ror__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__ror__(other)
	def __eq__(self, other):
		'''Forwards to the __eq__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__eq__(other)
	def __ne__(self, other):
		'''Forwards to the __ne__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__ne__(other)
	def __lt__(self, other):
		'''Forwards to the __lt__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__lt__(other)
	def __le__(self, other):
		'''Forwards to the __le__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__le__(other)
	def __gt__(self, other):
		'''Forwards to the __gt__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__gt__(other)
	def __ge__(self, other):
		'''Forwards to the __ge__ method of the underlying numpy array in the .data attribute.'''
		return self.data.__ge__(other)
	def __bool__(self, other):
		'''Forwards to the __bool__ method of the underlying numpy array in the .data attribute.'''
		self.data.__bool__()
	def __neg__(self, other):
		'''Forwards to the __neg__ method of the underlying numpy array in the .data attribute.'''
		self.data.__neg__()
	def __pos__(self, other):
		'''Forwards to the __pos__ method of the underlying numpy array in the .data attribute.'''
		self.data.__pos__()
	def __abs__(self, other):
		'''Forwards to the __abs__ method of the underlying numpy array in the .data attribute.'''
		self.data.__abs__()
	def __int__(self, other):
		'''Forwards to the __int__ method of the underlying numpy array in the .data attribute.'''
		self.data.__int__()
	def __float__(self, other):
		'''Forwards to the __float__ method of the underlying numpy array in the .data attribute.'''
		self.data.__float__()
	def __round__(self, other):
		'''Forwards to the __round__ method of the underlying numpy array in the .data attribute.'''
		self.data.__round__()
	def __ceil__(self, other):
		'''Forwards to the __ceil__ method of the underlying numpy array in the .data attribute.'''
		self.data.__ceil__()
	def __floor__(self, other):
		'''Forwards to the __floor__ method of the underlying numpy array in the .data attribute.'''
		self.data.__floor__()
	def __trunc__(self, other):
		'''Forwards to the __trunc__ method of the underlying numpy array in the .data attribute.'''
		self.data.__trunc__()

class MaskedDC(NumpyDC):
	'''A numpy data container which has numpy.ma.MaskedArray as underlying data structure.'''

	def __init__(self, *, data, **kwargs):

		if not isinstance(data, np.ma.MaskedArray):
			log.debug("Initialising %s with non-masked data.",
			          self.classname)
			data 	= np.ma.MaskedArray(data=data, mask=False)

		# Let the parent.__init__ perform saving of the masked array
		super().__init__(*args, data=data, **kwargs)

	# .........................................................................
	# Numpy related things

	# TODO allow easy access to the mask
	# NOTE might have to reconsider the DataContainer API, as it clashes with the MaskedArray attributes data and mask ...

# -----------------------------------------------------------------------------

class CfgContainer(DataContainer):
	'''A DataContainer specifically for config-file-like data structures. Allows access via attributes.'''

	def __init__(self, *, data, **kwargs):

		# Ensure its an EasyDict object
		data 	= EasyDict(data)

		super().__init__(data=data, **kwargs)

	def __getitem__(self, key):
		'''Returns the item from the data. If the given key is a list or tuple, traverses along these keys ...'''
		if not isinstance(key, (tuple, list)):
			return self.data[key]
		else:
			if len(key) > 1:
				obj 	= self.data
				for _key in key:
					# Walk through the dict
					obj 	= obj[_key]

				# Reached the leaf, return that value:
				return obj

			else:
				# Reached the leaf
				return self.data[key[0]]


# -----------------------------------------------------------------------------
class DataProxy:
	'''A data proxy fills in for the place of a dataset, e.g. if data should only be loaded on demand.'''

	@property
	def info_str(self):
		'''An info string that can be used to represent this object.'''
		return "proxy"

class H5DataProxy(DataProxy):
	'''The H5DataProxy fills in for HDF5 dataset. It stores the filename to get to the file and, on can, on demand, be resolved to the target class.'''

	def __init__(self, *, obj: h5.Dataset):
		'''Extracts the file name of the dataset object and the path to that dataset.'''

		self.fname 	= obj.file.filename
		self.name 	= obj.name

		self.shape 	= obj.shape
		self.dtype 	= obj.dtype

	@property
	def info_str(self):
		'''An info string that can be used to represent this object.'''
		return "proxy, {shape:}, {dtype:}".format(shape=self.shape,
		                                          dtype=self.dtype)

	def resolve(self):
		'''Resolve the proxy and load the data.'''

		with h5.File(self.fname, 'r') as h5file:
			return np.array(h5file[self.name])

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Helpers

def recursive_convert(data: DataContainer, *, match: str, MatchCls: DataContainer, TargetCls: DataContainer, **target_init_kwargs):
	'''Recursively goes through `data` and converts all entries that are of type MatchCls and having their name match with the regex `match` into TargetCls objects using the `.convert_to()` method of a DataContainer-derived object.
	'''

	log.debugv("recursive_convert called on %s '%s'",
	            data.__class__.__name__, data.name)

	# Loop through the data
	for key, obj in data.items():
		# Distinguish between group-like and container-like objects
		if isinstance(obj, DataGroup):
			# NOTE this check has to be performed first, as DataGroup is a child of DataContainer.

			# If it matches, convert it.
			if isinstance(obj, MatchCls) and re.fullmatch(match, key):
				log.debugv("Converting %s '%s' to %s ...", MatchCls.__name__, obj.name, TargetCls.__name__)
				data[key] 	= obj.convert_to(TargetCls, **target_init_kwargs)

			# Continue recursion, regardless of whether it matched or not
			data[key] = recursive_convert(data[key],
			                              match=match, MatchCls=MatchCls,
			                              TargetCls=TargetCls,
			                              **target_init_kwargs)

		elif isinstance(obj, DataContainer):
			# If it matches, convert it.
			if isinstance(obj, MatchCls) and re.fullmatch(match, key):
				log.debugv("Converting %s '%s' to %s ...", MatchCls.__name__, obj.name, TargetCls.__name__)
				data[key] 	= obj.convert_to(TargetCls, **target_init_kwargs)

			# Recursion cannot continue here -> done.

		else:
			# Nothing that will be convertable. Recursion cannot continue either. Do nothing.
			pass

	# Been through everything -> Return the updated data.
	return data
