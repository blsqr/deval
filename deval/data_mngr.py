#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''This module defines the DataManager, which is used to keep track of data
that should be loaded and evaluated together.'''

import os
import re
import glob
import copy
from datetime import datetime
from typing import Union
import yaml

import numpy as np
import h5py as h5

import deval
from deval.plot_hdlr import PlotHandler
from deval.data_cont import DataGroup, DataContainer, NumpyDC, CfgContainer, H5DataProxy

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants


# -----------------------------------------------------------------------------

class DataManager(DataGroup):
	'''A DataManager keeps track of data that belongs together. It extends the DataGroup class by some methods that are useful on the top-level of the data hierarchy -- such as loading the data -- and adds an interface to allow the evaluation and plotting of data.'''

	# Class constants
	IO_CFG 				= cfg.io
	DEFAULT_LOAD_CFG 	= {} # no defaults for base class
	DEFAULT_PLOT_CFG 	= {} # no defaults for base class
	PLOT_HANDLER_CLS 	= PlotHandler

	def __init__(self, data_dir: str, *, name: str=None, out_dir: str=None, load_cfg: Union[str, dict]=None, plot_cfg: Union[str, dict]=None, cfg_dir: str=None, **dg_kwargs):
		''' Initialises a DataManager object.

		Args:
			data_dir: 	the directory the data can be found in. If this is a
				relative path, it is considered relative to the current working directory.
			name: 		which name to give to the DataManager
			out_dir:  	where output is written to. If this is a relative path,
				it will be considered relative to the data directory
			**dg_kwargs: passed to DataGroup.__init__
		'''
		log.info("Initialising %s ...", self.__class__.__name__)

		# Find a name if none was given.
		if not name:
			name = "{}_Manager".format(os.path.basename(data_dir).replace(" ", ""))

		# Initialise via parent class.
		super().__init__(name=name, **dg_kwargs)

		# Initialise directories
		self._init_dirs(data_dir=data_dir, out_dir=out_dir, cfg_dir=cfg_dir)

		# If specific values for the load configuration and the plot configuration were given, use these to set the class constants of this instance.
		if load_cfg:
			if isinstance(load_cfg, str):
				# Assume this is the path to a configuration file and load the content of that file to use as defaults.
				log.debug("Using %s for the default load configuration.", load_cfg)
				self.DEFAULT_LOAD_CFG 	= deval.load_cfg_file(load_cfg)

			else:
				# Assume this is a mapping and use this without updating the defaults.
				log.debug("Using a given %s to use as default load configuration.", type(load_cfg))
				self.DEFAULT_LOAD_CFG 	= load_cfg

		if plot_cfg:
			if isinstance(plot_cfg, str):
				# Assume this is the path to a configuration file and load the content of that file to use as defaults.
				log.debug("Using %s for the default load configuration.", plot_cfg)
				self.DEFAULT_PLOT_CFG 	= deval.load_plt_cfg(plot_cfg)

			else:
				# Assume this is a mapping and use this without updating the defaults.
				log.debug("Using a given %s to use as default load configuration.", type(plot_cfg))
				self.DEFAULT_PLOT_CFG 	= plot_cfg

		# Done
		log.note("%s initialised.", self.__class__.__name__)

	def _init_dirs(self, data_dir: str, out_dir: str=None, cfg_dir: str=None):
		'''Initialises the directories.'''

		# Make the data directory absolute
		log.debugv("Data directory (rel.):\n  %s", data_dir)
		data_dir 	= os.path.abspath(data_dir)
		log.debugv("Data directory (abs.):\n  %s", data_dir)

		# Make current date and time available for formatting operations
		datestr		= datetime.now().strftime(self.IO_CFG.datestrf)

		# Output directory . . . . . . . . . . . . . . . . . . . . . . . . . .
		if not out_dir:
			# Use the default_out_dir

			# Create a directory below the data directory with a default name, determined in the configuration
			out_dir 	= self.IO_CFG.default_out_dir

		# Perform a format operation
		out_dir 	= out_dir.format(name=self.name.lower(), date=datestr)

		# If it is relative, assume it to be relative to the data directory
		if not os.path.isabs(out_dir):
			out_dir 	= os.path.join(data_dir, out_dir)


		# Config directory . . . . . . . . . . . . . . . . . . . . . . . . . .
		if not cfg_dir:
			# No path given; use defauult
			cfg_dir 	= self.IO_CFG.default_cfg_dir

		# Perform a format operation
		cfg_dir 	= cfg_dir.format(name=self.name.lower(), date=datestr)

		if not os.path.isabs(cfg_dir):
			cfg_dir 	= os.path.join(out_dir, cfg_dir)

		# Save to attributes
		self.dirs 	= dict(data=data_dir, out=out_dir, cfg=cfg_dir)

		# Make paths absolute, then create the directories
		for key, path in self.dirs.items():
			self.dirs[key] 	= os.path.abspath(path)
			os.makedirs(self.dirs[key], exist_ok=True)

		log.state("Managed directories:\n%s",
		          deval.tree_printer(self.dirs, key_length=6))

	# .........................................................................
	# Special Methods

	# .........................................................................
	# Loading data

	def load_data(self, load_cfg: dict=None, update_load_cfg: dict=None, overwrite_existing: bool=True, log_data: bool=False) -> None:
		''' Load the data using the specified load_cfg. If not specified, the class-level defaults are used.
		By passing the `update_load_cfg` argument, the load_cfg (be it the passed one or the defaults) is recursively updated with the passed dictionary.

		If `overwrite_existing` is True, data entries with the same key will be overwritten.'''

		# Make sure to work on a copy, be it on the defaults or on the passed
		if not load_cfg:
			log.debug("Using default load_cfg.")
			load_cfg 	= copy.deepcopy(self.DEFAULT_LOAD_CFG)

		else:
			load_cfg 	= copy.deepcopy(load_cfg)

		if update_load_cfg:
			# Recursively update with the given keywords
			load_cfg 	= deval.tools.rec_update(load_cfg, update_load_cfg)
			log.debug("Updated the default load configuration for this call of `load_data`.")

		log.highlight("Loading %d data entries ...", len(load_cfg))

		# Loop over the data entries that were configured to be loaded.
		for key, params in load_cfg.items():
			log.note("Loading data entry '%s' ...", key)

			# Warn if the key is already present
			if key in self:
				if overwrite_existing:
					log.warning("The data entry '%s' was already loaded and will be overwritten.", key)
				else:
					log.caution("The data entry '%s' was already loaded; not loading it again ...", key)
					continue

			# Extract the group name, which is needed below and need not be supplied to the load function
			group_name = params.pop('group_name', None)

			# Try loading the data and handle exceptions that map to specific scenarios
			try:
				_data 	= self._data_loader(name=key, **params)
				# NOTE this is the direct return value of the data loader or -- if a group was required -- the resulting group.

			except deval.exc.RequiredDataMissingError:
				log.error("'%s' could not be loaded, but was required.", key)
				raise

			except deval.exc.MissingDataError:
				log.caution("No files were found to import.")
				# Does not raise
				_data 	= None

			except deval.exc.DataManagerError:
				# Loader was not available.
				raise

			else:
				# Everything as desired
				log.debug("Data successfully imported.")

			# Save the data
			# See if a base_group was specified in the parameters. If yes, do not load the data under self[key] but into the base group with the specified name. If that group is not present, create it.
			if not group_name:
				# Save it under the key of this data entry
				self[key] 	= _data
				log.progress("Imported and saved data to key '%s'.", key)

			else:
				# Find the group; create it if necessary
				if group_name in self:
					group 		= self[group_name]
				else:
					log.note("Creating group '%s' ...", group_name)
					group 		= DataGroup(name=group_name)
					self[group_name] = group

				# Recursively save to that group
				group.rec_update(_data)
				log.progress("Imported and saved data into group '%s'.", group_name)

			# Done with this config entry, continue with next
		else:
			# All done
			log.info("Successfully loaded %d data entries.", len(self.data))
			log.state("Available data entries:\n  %s",
			          ",  ".join(self.data.keys()))

		if log_data:
			log.state("Data tree:\n%s", str(self))

		# Done
		return

	def _data_loader(self, *, name: str, loader: str, glob_str: str, exclude: list=None, required: bool=False, always_create_group: bool=False, name_pattern: str=None, try_int_cast: bool=False, leaf_group_name: str=None, load_indicator: bool=False, in_parallel: bool=False, **loader_kwargs) -> DataContainer:
		'''Loads an entry using the given parameters.

		Args:
			loader:
			 	Name of the loader method to use. The method needs to
				be an attribute of the class and have the prefix '_load_'
			glob:
				To identify the file to load inside the data directory.
			exclude:
			 	Iterable of strings to exlude from the glob result
			required:
			 	If True, raises an error if this entry could not be loaded or was empty
			always_create_group:
				If the given `glob_str` matches more than one file, this argument being true ensures that a group is still created.
			name_pattern:
				A regex pattern that is used to extract the name of the leaf entry
			try_int_cast:
				If true, it is tried to cast the result of the name_pattern regex to an integer
			leaf_group_name:
				If given, the data of each file is loaded into a subgroup with that name
			load_indicator:
				If True, shows a progress indicator
			in_parallel:
				If there is more than one files, loads the files in multiple processes -- Not implemented yet! # TODO
			**loader_kwargs:
			 	Passed on to the loader method

		Returns:
			corresponding data (variable type)

		Raises:
			deval.exc.DataManagerError: Loader was not available
			deval.exc.MissingDataError: Data was missing, but not required
			deval.exc.RequiredDataMissingError: Data was missing and was required
		'''

		def extract_key_from_fpath(file, *, group, ptrn=None, cast_to_int: bool=False):
			'''Helper method to extract the name from a key.'''
			if ptrn:
				# Use the specified regex pattern to extract the first group
				try:
					key = re.findall(ptrn, file)[0]
				except IndexError as err:
					log.warning("Could not extract a key using the pattern '%s' on the file path:\n%s\nUsing the hash instead.", ptrn, file)
					key = hash(file)[:12]

			else:
				# name the entries by the file's name (without extension)
				key = os.path.splitext(os.path.basename(file))[0].lower()

				if key in group:
					log.warning("Duplicate file name '%s' in %s, adding part of the hash of the file path '%s' to the group name ...", key, type(group), file)
					key += "_" + str(hash(file))[:8]

			# There is the option that the key might be castable to integer; try this:
			if cast_to_int:
				try:
					_key 	= int(key)
				except ValueError as err:
					log.warning("Cast to int failed. ValueError: %s", err)
				else:
					log.debugv("Cast to int succeeded: %s -> %s", key, _key)
					key 	= _key

			return key

		# Get the load function
		load_func 	= getattr(self, '_load_' + loader)
		if not load_func:
			log.error("Loader %s is not available. Make sure %s has a method '%s'.", loader, self.__class__.__name__, '_load_' + loader)
			return deval.exc.DataManagerError("Loader '{}' was not available.".format(loader))

		glob_str 	= os.path.join(self.dirs['data'], glob_str)
		log.debugv("Created absolute glob string:\n  %s", glob_str)
		files 	= glob.glob(glob_str, recursive=True)

		if exclude:
			while exclude:
				rmf 	= exlude.pop()
				try:
					files.remove(rmf)
				except ValueError:
					# not in list
					log.warning("%s was not in glob list.", rmf)
				else:
					log.debugv("%s removed from glob list.", rmf)

		log.debugv("Found %d files for loader '%s':\n%s",
		          len(files), loader, "\n".join(files))

		if not files:
			if not required:
				log.debug("No files to import.")
				raise deval.exc.MissingDataError("No files to import.")

			else:
				log.warning("No files matching '%s' and exluding '%s' were found, but were required.", glob_str, exclude)
				raise deval.exc.RequiredDataMissingError("No files for the required data were found.")

		# Else: there was a file to load.

		# If a regex pattern was specified, compile it
		dk_ptrn = re.compile(name_pattern) if name_pattern else None

		# Distinguish between cases where a group should be created and one where the DataContainer-object can be directly returned
		if len(files) == 1 and not always_create_group:
			# Use the single file available
			file 	= files[0]

			# Find the name and the key
			dkey	= extract_key_from_fpath(file, group=self,
			                                 ptrn=dk_ptrn,
			                                 cast_to_int=try_int_cast)

			# Load the data
			data 	= load_func(file, name=dkey, **loader_kwargs)

			# And return it
			return data

		else:
			# More than one file -> need to work with groups. This depends on whether the tree should be inverted or not.

			if in_parallel:
				# TODO
				raise NotImplementedError("Cannot load in parallel yet.")

			# Create a base group, that will serve as the container for all return values.
			group 	= DataGroup(name=name)

			# Go over the files and load them
			# TODO parallelise this for loop
			for n, file in enumerate(files):
				if load_indicator:
					line 	= "  Loading ... {}/{}".format(n+1, len(files))
					print(deval.tools.fill_tty_line(line), end="\r")

				# Need to specify a name; either that is extracted from the whole filepath with the specified regex, or the filename is used.
				dkey 	= extract_key_from_fpath(file, group=group,
				                                 ptrn=dk_ptrn,
				                                 cast_to_int=try_int_cast)

				# Use the load_func to get the data
				_data 	= load_func(file, name=dkey, **loader_kwargs)

				# Distinguish where to put the loaded data
				if leaf_group_name:
					# Create a subgroup and put the data inside under the leaf group name. For consistency, the name of the subgroup is dkey.
					subgroup 	= DataGroup(name=dkey)
					subgroup[leaf_group_name] 	= _data
					_data 		= subgroup

				# Save to the return group
				group[dkey] = _data

			return group

	# Specific loaders  . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _load_yml(self, file: str, *, name: str, **yaml_load_kwargs) -> CfgContainer:
		'''Loads a yml file.'''
		log.debug("Loading YAML file %s ...", file)

		with open(file, 'r') as f:
			data 	= yaml.load(f, **yaml_load_kwargs)

		return CfgContainer(name=name, data=data)

	def _load_yaml(self, *args, **kwargs):
		return self._load_yml(*args, **kwargs)

	def _load_hdf5(self, file: str, *, name: str, load_on_demand: bool=False, lower_case_keys: bool=True, GroupCls: Union[str,DataGroup]=DataGroup, DsetCls: Union[str,NumpyDC]=NumpyDC, print_params: dict=None) -> DataGroup:
		'''Loads the specified hdf5 file into DataGroup and DataContainer-like object; this completely loads all data into memory and recreates the hierarchic structure of the hdf5 file.

		The h5py File and Group objects will be converted to the specified DataGroup-derived objects; the Dataset objects to the specified DataContainer-derived object.

		All attributes are carried over and are accessible under the `attrs` attribute.

		Args:
			file: 			hdf5 file to load
			name: 			the group this is loaded into
			load_on_demand:	if True, the leaf datasets are only loaded when their .data attribute is accessed the first time. To do so, a reference to the hdf5 file is saved in a H5DataProxy
			lower_case_keys: whether to cast all keys to lower case
			GroupCls: 		class to use to substitute the h5.Group objects
			DsetCls: 		class to use to substitute the h5.Dataset objects
			print_params:
				level: 		how verbose to print loading info (0: None, 1: file level, 2: every level, recursively)
				fstrs1: 	format string level 1
				fstrs2: 	format string level 2
		'''

		def recursively_load_hdf5(src, target: DataGroup, *,lower_case_keys: bool=False, GroupCls: Union[str,DataGroup]=DataGroup, DsetCls: Union[str,DataContainer]=NumpyDC):
			'''Recursively loads the data from the source hdf5 file into the target DataGroup object.'''

			# Go through the elements of the source object
			for key, obj in src.items():
				if lower_case_keys and isinstance(key, str):
					key 	= key.lower()

				if isinstance(obj, h5.Group):
					# Create a group in the target object, carry over the attributes, and then start the recursive call
					target[key] = GroupCls(name=key, attrs=obj.attrs)
					recursively_load_hdf5(obj, target[key],
					                      lower_case_keys=lower_case_keys,
					                      GroupCls=GroupCls, DsetCls=DsetCls)

				elif isinstance(obj, h5.Dataset):
					# Reached a leaf -> Import the data and attributes into a DataContainer-derived object. As a basic data structure, a np.array is assumed.
					if plvl >= 2:
						line 	= fstr2.format(name=name, key=key, obj=obj)
						print(deval.tools.fill_tty_line(line), end="\r")

					if not load_on_demand:
						# Import the data completely
						data 		= np.array(obj)
					else:
						data 		= H5DataProxy(obj=obj)
					target[key] = DsetCls(name=key, data=data, attrs=obj.attrs)

				else:
					log.warning("Encountered %s during loading of hdf5 file; this is not supported yet.", type(obj))

		# Prepare print format strings
		print_params = print_params if print_params else {}
		plvl  = print_params.get('level', 0)
		fstr1 = print_params.get('fstr1', "  Loading {name:} ... ")
		fstr2 = print_params.get('fstr2', "  Loading {name:} - {key:} ...")

		# Resolve the specified group class and dataset class arguments
		# TODO make string-resolvable, to allow specification in config file. Alternatively, add yml tag `!cls` to resolve module strings to classes directly during reading of the config file
		if isinstance(GroupCls, str):
			raise NotImplementedError("Cannot get GroupCls by str yet.")

		if isinstance(DsetCls, str):
			raise NotImplementedError("Cannot get DsetCls by str yet.")


		# Initialise the root group
		log.debug("Loading hdf5 file %s into %s ...", file, GroupCls.__name__)
		group 	= GroupCls(name=name)

		# Now recursively go through the hdf5 file and add them to the group
		with h5.File(file, 'r') as h5file:
			if plvl >= 1:
				line 	= fstr1.format(name=name, file=file)
				print(deval.tools.fill_tty_line(line), end="\r")
			recursively_load_hdf5(h5file, group,
			                      lower_case_keys=lower_case_keys,
			                      GroupCls=GroupCls, DsetCls=DsetCls)

		return group

	# .........................................................................
	# Plotting

	def perform_plots(self, plot_cfg: dict=None, update_plot_cfg: dict=None, handler_cls=None, dump_plot_cfg: bool=True) -> PlotHandler:
		'''Performs the plots specified in the plot_cfg. If no plot_cfg is given, takes the default plot configuration from the class variable DEFAULT_PLOT_CFG. With `update_plot_cfg`, the previously determined `plot_cfg` is updated, regardless of this being the default or a custom passed configuration.

		The actual plotting is managed by a PlotHandler-derived class, that can be set via argument; if none is set, the default handler for this DataManager is used, specified in the PLOT_HANDLER_CLS class variable.'''

		if not plot_cfg:
			log.debug("Using default plot_cfg.")
			plot_cfg 		= self.DEFAULT_PLOT_CFG

		if update_plot_cfg:
			# Need to work on a copy
			plot_cfg 		= copy.deepcopy(plot_cfg)

			# Recursively update with the given keywords
			deval.tools.rec_update(plot_cfg, update_plot_cfg)
			log.debug("Updated the plot configuration for this call.")

		if not handler_cls:
			handler_cls 	= self.PLOT_HANDLER_CLS
			log.debug("Using default plot handler class %s.",
			          handler_cls.__class__.__name__)

		# Save the plot configuration to the output directory as yml
		if dump_plot_cfg:
			# Create the path and if needed the directories
			_fstr 		= self.IO_CFG.final_plot_cfg_fstr
			dump_fpath 	= os.path.join(self.dirs['cfg'],
			                           _fstr.format(name=self.name))
			os.makedirs(os.path.dirname(dump_fpath), exist_ok=True)

			# TODO recursively convert the EasyDict to a regular dict

			with open(dump_fpath, 'w') as dump_file:
				yaml.dump(plot_cfg, dump_file)
				log.state("Dumped final plot configuration to config directory.\n  %s", dump_fpath)

		# Create plot handler with the current manager as data source
		plot_handler 	= handler_cls(dm=self)

		# Pass on to plot handler, which parses the parameters, gets the desired plotting function, starts and saves the figure ...
		plot_handler.perform_plots(plot_cfg)

		return plot_handler

