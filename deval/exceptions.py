#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Exception classes for deval'''

import logging

# NOTE to avoid circular imports, this file may not access any other parts of the deval package.

# Setup logging for this file
log	= logging.getLogger(__name__)
log.debug("Loaded module and logger.")

# Local constants

# -----------------------------------------------------------------------------
# Exceptions during plotting

class PlottingError(Exception):
	'''A PlottingError is raised by the PlotHandler in the case of an error during plotting.'''

	def __init__(self, message, plot_params=None):

		# Initialise the base class
		super().__init__(message)

		# Custom code
		self.plot_params 	= plot_params

	# TODO can do more here

# -----------------------------------------------------------------------------
# DataManager-specific exception handling

class DataManagerError(Exception):
	'''For general errors raised in the DataManger.'''
	pass

class MissingDataError(DataManagerError):
	'''Raised when data is missing.'''
	pass

class RequiredDataMissingError(MissingDataError):
	'''Raised when data is missing that was required.'''
	pass
