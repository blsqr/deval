#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Plotting functions'''

import os
import copy

from typing import Union, Tuple

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import deval

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Try to import seaborn
try:
	import seaborn as sns

except ImportError:
	log.warning("Seaborn could not be imported; some plotting functionality will not be available.")
	if deval.DEBUG:
		raise

else:
	log.debugv("seaborn imported successfully")

# Local constants

# -----------------------------------------------------------------------------

def plt_lineplot(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, y_data_key: [str], x_data_key: [str]=None, lines_from: str='cols', row_slice: list=None, col_slice: list=None, label_fstr: str=None) -> None:
	'''A general method to perform a lineplot of 1- or 2-dimensional data.

	Args:
		y_data_key: the key to get to the data that will be plotted on the x-axis.
		x_data_key: the key to get to the data that will be used for the common (!) x-axis. Optional.
		lines_from: Whether to create the lines from the columns (`cols`) or rows (`rows`) of the data
		row_slice: arguments from which to generate a slice object that will be used on the rows. If `None`, the whole slice is selected.
		col_slice: arguments from which to generate a slice object that will be used on the columns If `None`, the whole slice is selected.
		style:
			use_cmap: whether the lines should get the color from the color cycler or if the cmap specified in `plot_kwargs` should be used.

	Raises:
		deval.exc.PlottingError
	'''

	# Get data
	log.debug("Selecting data from keys ...\n  x: %s\n  y: %s",
	         x_data_key, y_data_key)
	x_data = dm[x_data_key].data if x_data_key else None
	y_data = dm[y_data_key].data

	# Compile generators for the original row and column numbers, because it can change due to the selection happening below
	row_nos 	= range(y_data.shape[0])
	col_nos 	= range(y_data.shape[1])

	# Can select a subset of rows or columns
	if row_slice is not None:
		row_slice 	= slice(*row_slice)
		y_data 		= y_data[row_slice,:]
		log.debug("selected rows with slice: %s", row_slice)

	if col_slice is not None:
		col_slice 	= slice(*col_slice)
		y_data 		= y_data[:,col_slice]
		log.debug("selected columns with slice: %s", col_slice)

	log.debug("Data shapes:   x: %s  –  y: %s",
	          x_data.shape, y_data.shape)

	# Distinguish dimensions
	if len(y_data.shape) == 1:
		# Just plot as a line
		if x_data:
			plt.plot(x_data, y_data)
		else:
			plt.plot(y_data)

	elif len(y_data.shape) == 2:
		# Determine whether to plot from columns or from rows
		if lines_from in ['row', 'rows']:
			# Plot each column as a line
			num_lines 	= y_data.shape[0]
			sel 		= lambda n: (n, slice(None))
			label_nums	= row_nos[row_slice]
			if not label_fstr:
				label_fstr 	= "Row {i:d}"

		elif lines_from in ['cols', 'columns']:
			# Plot each row as a line
			num_lines 	= y_data.shape[1]
			sel 		= lambda n: (slice(None), n)
			label_nums	= col_nos[col_slice]
			if not label_fstr:
				label_fstr 	= "Column {i:d}"

		else:
			raise deval.exc.PlottingError("Invalid argument lines_from: '{}'".format(lines_from))

		for n in range(num_lines):
			# Prepare the _kws
			_kws 			= dict()
			_kws['label'] 	= label_fstr.format(i=label_nums[n], n=n)

			# Get the line data
			line 			= y_data[sel(n)]

			if style.get('use_cmap', False):
				# Use the color from the color map; if this kwarg is not set here, the color cycler will determine the color.
				_kws['color']	= cmap(n/num_lines)

			# Perform the lineplot
			if x_data:
				plt.plot(x_data, line, **_kws)
			else:
				plt.plot(line, **_kws)

		log.debug("Plotted %d lines.", num_lines)

	else:
		raise deval.exc.PlottingError("Unsupported data shape: "+str(data.shape))
	# Done here.
	return
plt_lineplot.style 	= cfg.plot_styles.lineplot

def plt_template(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, **function_specific_kwargs) -> None:
	'''This is a template method that can be copied and used to generate new plotting methods.'''

	# Do things here.
	# Data is available through the data manager, dm

	return
plt_template.style 	= cfg.plot_styles.template


# -----------------------------------------------------------------------------
# .............................................................................
# -----------------------------------------------------------------------------
# .............................................................................
# -----------------------------------------------------------------------------
# .............................................................................
# -----------------------------------------------------------------------------
# .............................................................................
# -----------------------------------------------------------------------------
# Plot Helpers
# These do not have to adhere to the template call signature

def mean_and_error_bands(*, ax, mean, low=None, high=None, x=None, label: str=None, color: str=None, mean_kwargs: dict=None, errb_kwargs: dict=None, wrpr=None):
	'''A combined plotting method for the mean and error bands. This adds a custom legend handle that adheres to the combined style.'''

	if x is None:
		log.debug("No x data given for mean_and_error_bands; using integer steps.")
		x 	= range(len(low))

	log.debug("Plotting mean and error bands ...")

	# Gather kwargs
	mean_kwargs 	= mean_kwargs if mean_kwargs else {}
	errb_kwargs 	= errb_kwargs if errb_kwargs else {}

	# See if there was a color defined; if yes, add it to the kwargs
	if color is not None:
		mean_kwargs['color'] 	= color
		errb_kwargs['color'] 	= color

	# Prepare the common label value, if supplied.
	if label and wrpr:
		# There is a common label -> remove the individual label entries
		if 'label' in mean_kwargs:
			del(mean_kwargs['label'])
		if 'label' in errb_kwargs:
			del(errb_kwargs['label'])

	elif label and not wrpr:
		# Cannot do it, because have no proper access to the legend to register custom handles – adding it directly is cumbersome and error-prone.
		log.error("Cannot add common legend label for mean and std without the corresponding PlotWrapper being supplied.")
		label 	= None

	# Plot the mean
	line, 	= ax.plot(x, mean, **mean_kwargs)
	# returns a Line2D as first element of an iterable; save that to use for the legend handle

	if color is None:
		# Extract the color from the line drawn in order to let the error bands and the line have the same color
		errb_kwargs['color'] 	= line.get_color()

	# Plot the error bands
	if low is not None or high is not None:
		polycoll = error_bands(low=low, high=high, x=x, ax=ax, **errb_kwargs)
	else:
		polycoll = None
	# returns a PolyCollection as only return value; save that to use for the legend handle

	# If a label was specified, merge the handles of line and error bands and then add that entry to the legend
	if label:
		log.debugv("Adding custom legend entry with label '%s'.", label)

		# Create the merged handle
		if polycoll is None:
			_handle = line
		else:
			_handle = (line, polycoll)

		# Call the wrappers registration function; this will take care of later on adding the legend entry
		wrpr.register_legend_handle(ax=ax, handle=_handle, label=label)

	return line, polycoll

def error_bands(*, low, high, x=None, ax=None, **fill_kwargs):
	'''Plots the error bands for a given axis.'''
	if not ax:
		ax 	= plt.gca()

	if x is None:
		log.debug("No x data given for error_bands; using integer steps.")
		x 	= range(len(low))

	return ax.fill_between(x, low, high, **fill_kwargs)
