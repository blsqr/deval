#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' This module holds the PlotHandler class, a handler associated with a DataManager class that coordinates the plotting. It performs the reading of the plotting configuration, calls the appropriate plot wrappers with the corresponding plot functions, saves the figure, performs looping, ...'''

import os
import copy
from time import sleep

try:
	import dill as pkl
except ImportError:
	# Fallback option pickle
	import pickle as pkl
	_dill_failed 	= True
else:
	_dill_failed 	= False

from typing import Union, Tuple

import matplotlib.pyplot as plt # needed for saving etc.

import deval
import deval.plot_wrapr

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Inform about dill import status
if _dill_failed:
	log.warning("Could not import dill; used the less capable pickle package as fallback option.")
del(_dill_failed)

# Local constants

# -----------------------------------------------------------------------------

class PlotHandler:
	'''The plot handler takes care of generating a plot from given data and a plot configuration.'''

	# Class constants
	PLOT_WRAPPERS 		= deval.plot_wrapr # where to look for plot wrappers
	DEFAULT_WRAPPER_NAME= cfg.default_wrapper_name
	FNAME_FSTRS 		= cfg.fname_fstrs

	def __init__(self, dm, *, out_dir: str=None, plot_wrapper_mod=None):
		'''Initialises the PlotHandler object.

		Args:
			dm 			The DataManager-derived object which supplies the data.
			out_dir:	If given, this will be used as the base output directory. If not, the output directory of the DataManager is used.

		'''
		# Store the attributes
		self.dm 	= dm

		# The out_dir
		if out_dir:
			self.out_dir 	= out_dir
		else:
			self.out_dir 	= self.dm.dirs['out']

		# If given, change the class constant for the plot wrapper module
		if plot_wrapper_mod:
			if isinstance(plot_wrapper_mod, str):
				raise NotImplementedError("Cannot pass plot wrapper module by module string yet; need to pass resolved module directly.")

			self.PLOT_WRAPPERS 	= plot_wrapper_mod
			log.debug("Updated plot wrapper module: %s", self.PLOT_WRAPPERS)

		log.note("%s initialised for %s '%s'.", self.__class__.__name__,
				  self.dm.__class__.__name__, self.dm.name)

	# .........................................................................
	# Methods to perform plots

	def perform_plots(self, plot_cfg: dict):
		'''Performs the plots specified in the plot_cfg.'''

		# Work on a copy of the given dictionary, just to be on the safe side
		plot_cfg 	= copy.deepcopy(plot_cfg)

		log.info("%d plot configurations found ...\n", len(plot_cfg))
		log.state("%s\n",  ",  ".join(list(plot_cfg.keys())))
		cntr		= dict(success=0, skipped=0, failed=0)

		# Go over the plot config entries
		for name, params in plot_cfg.items():
			# Work on a copy of the params dictionary, to be on the safe side
			params = copy.deepcopy(params)

			# Check if this plot should be performed or skipped
			if params.pop('enabled', True):
				log.highlight("Plotting '%s' ...", name)
			else:
				log.highlight("Skipping '%s' ...", name)
				cntr['skipped'] += 1
				continue

			# Pass the remaining parameters to the method that handles a single plot
			try:
				self.perform_plot(name=name, **params)

			except KeyboardInterrupt:
				sleep_time 	= cfg.sleep_time
				log.warning("Received KeyboardInterrupt. Aborting '%s' plot.\n\nPerform KeyboardInterrupt again (within %ss) to stop plotting altogether.\n", name, sleep_time)

				try:
					sleep(sleep_time)
				except KeyboardInterrupt:
					log.warning("Received another KeyboardInterrupt. Aborting all remaining plots.\n\nPerform KeyboardInterrupt again (within %ss) to re-raise.\n", sleep_time)
					try:
						sleep(sleep_time)
					except KeyboardInterrupt:
						raise
					else:
						break
				else:
					continue

			except deval.exc.PlottingError as err:
				# Some kind of failure below. Note that every exception from the plotting function is converted into deval.exc.PlottingError in the perform_plot method.
				cntr['failed'] 	+= 1
				msg	= "{} raised during '{}' plot:\n\n{}".format(err.__class__.__name__, name, err)

				if deval.DEBUG:
					# re-raise to get a traceback
					log.error(msg)
					raise

				else:
					# Inform about error and option to get a traceback
					log.error(msg + "\nContinuing with the other plots ...\n(Set the package-wide DEBUG flag to see the traceback.)\n")

			else:
				# Done with this plot.
				cntr['success'] += 1
				pass
		else:
			# All done
			log.info("Finished all configured plots.")
			log.note("(%s)", ",  ".join(["{}: {}".format(k, v)
			                             for k, v in cntr.items()]))

	def perform_plot(self, *, name: str, func_name: str, wrapper: dict=None, style: dict=None, search_modstr: str=None, fname_fstrs: dict=None, **func_kwargs):
		'''Performs a single plot with the given name, using the specified func_name'''

		# Check defaults
		if not fname_fstrs:
			log.debug("Using default fname_fstrs.")
			fname_fstrs 	= self.FNAME_FSTRS
		else:
			log.debug("Got fname_fstrs passed.")
		log.debugv("fname_fstrs:\n%s", fname_fstrs)

		# Initialise the PlotWrapper and prepare plotting . . . . . . . . . . .
		wrapper_cfg	= wrapper if wrapper else {}
		wrapper 	= self.get_wrapper(plot_name=name,
		                               plot_func_name=func_name,
		                               search_modstr=search_modstr,
		                               func_kwargs=func_kwargs,
		                               update_style=style,
		                               **wrapper_cfg)

		# Let the wrapper prepare for the plotting
		wrapper.prepare_plotting()


		# Prepare loop variables . . . . . . . . . . . . . . . . . . . . . . .
		loop_plot 	= True 	# whether to continue looping
		rv 			= None 	# the return value of plot_func, passed back to it
		n 			= 0 	# the iteration number of the current plot loop

		# Allow to perform this in a loop, depending on the return value of the plot_func
		# NOTE have to use a while loop here, because it is left to the plot_func to determine whether there should be iterations or not; there is no easy way of knowing this beforehand.
		# NOTE that this is not the loop to generate subplots! That needs to be done in the plot func.
		while loop_plot:
			# Conservative approach: always work on copies or distached elements of the dicts, because someone could change these in plot_func and therefore mess things up ...
			_fname_fstrs= copy.deepcopy(fname_fstrs)

			# Enter a try-except context, as all the following can raise StopIteration to indicate.
			try:
				# Prepare for the single plot
				wrapper.prepare_single_plot(old_rv=rv)

				# Let the wrapper create the figure
				wrapper.create_figure()

				# Pass all to the plotting function and let it do the plotting. Save the return value -- this will determine whether the loop will continue or not -- and pass the return value of the previous iteration to this new function call
				wrapper.perform_plot()

				# Perform postprocessing on the plot using the helper methods
				wrapper.postprocess_single_plot()

			except StopIteration:
				# Break out of the while loop immediately
				log.debugv("'%s' raised StopIteration. Not continuing plotting…", func_name)
				break

			except Exception as err:
				# Catch all other exceptions and transform into deval.exc.PlottingError
				msg	= "Uncaught exception within wrapper '{wrapper:}'!\n\n"\
				      "{ecls:}: {err:}\n".format(ecls=err.__class__.__name__,
				                                 err=err,
				                                 wrapper=wrapper.classname)

				raise deval.exc.PlottingError(msg) from err

			else:
				log.debug("Finished plot.")

			log.debug("Acquiring loop iterator state ...")
			rv 	= wrapper.loop_rv

			# Check for the case for not StopIteration is called but there was just no return value
			if not rv:
				# Finish this iteration and then go out of the while loop
				loop_plot 	= False

			else:
				# Looping will continue
				log.debugv("Got return value from '%s':\n%s", func_name, rv)

				# Use the format string configurations from the return value to update the default format strings
				_fname_fstrs.update(rv.get('fname_fstrs', {}))
				# TODO recursive update helpful?

				# Increment n for next loop iteration
				n += 1


			# Saving . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
			# Generate a filename for saving
			if not loop_plot and n == 0:
				# Case of no loops being carried out -> do not change the pre- and suffix
				_prefix = _suffix = ""

			else:
				# Loops will be carried out
				log.debug("Adjusting prefix and suffix ...")

				# Create a dict with the arguments for the .format method below
				fd 		= dict(n=n, name=name)

				# See if there is additional information in the rv dict
				if rv.get('n_max'):
					fd['n_max'] 	= rv['n_max']
				if rv.get('n_digits'):
					fd['n_digits'] 	= rv['n_digits']

				log.debugv("Using format dictionary to create prefix and suffix strings...\n  %s", fd)

				# If given by the return value, take these instead. Then apply the format operation
				_prefix = _fname_fstrs.get('prefix',
				                           _fname_fstrs['prefix']).format(**fd)
				_suffix = _fname_fstrs.get('suffix',
				                           _fname_fstrs['suffix']).format(**fd)

			# Now, having prefix and suffix defined, create the file name and join it together with he output directory to have a file path
			fname 	= _fname_fstrs['chain'].format(name=name, prefix=_prefix, suffix=_suffix)
			fpath 	= os.path.join(self.out_dir, fname)

			# Make sure that all needed directories exist
			log.debugv("Creating directories ...")
			os.makedirs(os.path.dirname(fpath), exist_ok=True)

			# Save the figure
			wrapper.save_figure(fpath_fstr=fpath) # NOTE still a format string with key `ext`

			# Close the figure
			wrapper.close_figure()

			# Finishing up the loop iteration . . . . . . . . . . . . . . . . .
			# If this is a loop iteration, inform about it
			if loop_plot:
				if rv.get('n_max'):
					log.info("Finished plot %d / %d\n", n, rv.get('n_max'))
				else:
					log.info("Finished plot %d / ?\n", n)

			# Done with this loop . . . . . . . . . . . . . . . . . . . . . . .
		# Exited loop
		# Finish up
		wrapper.postprocess_plotting()

		# Done.
		log.info("Finished plotting '%s'.\n", name)
		return

	# .........................................................................
	# Helpers

	def get_wrapper(self, *, wrapper_name: str=None, **wrapper_kwargs):
		'''Gets a wrapper by the given name'''
		# TODO check if PLOT_WRAPPERS class constant is necessary or if this could be done with a modstr

		if not wrapper_name:
			log.debug("Using default wrapper ...")
			wrapper_name 	= self.DEFAULT_WRAPPER_NAME

		log.debug("Getting plot wrapper class by name: '%s' ...", wrapper_name)
		WrapperCls	= getattr(self.PLOT_WRAPPERS, wrapper_name)
		# TODO exception handling

		# Initialise it
		log.note("Initialising plot wrapper:  %s ...", WrapperCls.__name__)
		return WrapperCls(handler=self, **wrapper_kwargs)

