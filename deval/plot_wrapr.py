#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' PlotWrapper-derived classes help automating some plotting.'''

import os
import copy
from functools import partial
from typing import Union, Tuple, Sequence
from pkg_resources import resource_filename

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms

import deval

# Setup logging for this file
log	= deval.get_logger(__name__)
cfg = deval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Try to import seaborn
try:
	import seaborn as sns

except ImportError:
	log.warning("Seaborn could not be imported; some plotting functionality will not be available.")
	if deval.DEBUG:
		raise

else:
	log.debugv("seaborn imported successfully")

# Local constants

# -----------------------------------------------------------------------------

class PlotWrapper:
	'''The base PlotWrapper class.
	TODO write docstring
	'''
	# Where to look for the plot functions (module string)
	PLOT_FUNC_MODSTR 	= cfg.plot_func_modstr

	# Base plot style
	BASE_STYLE 			= cfg.base_style  # the base plot style
	UPDATE_BASE_STYLE 	= {} # child classes can use this for style update
	DEFAULT_RC_FILE     = resource_filename("deval", "default_rc.yml")

	def __init__(self, *, plot_name: str, handler, plot_func_name: str, func_kwargs: dict=None, search_modstr: str=None, update_style: dict=None):

		# Save arguments to attributes
		self._plot_name		= plot_name
		self._handler 		= handler
		self._plot_func 	= None
		self._plot_func_name= plot_func_name
		self._func_kwargs 	= func_kwargs

		# Get the plot function
		self._init_plot_func(modstr=search_modstr)

		# Get the style
		self._style = None
		self._init_style(update_with=update_style)

		# Plotting-related attributes can all be saved in a common dictionary that is accessible by PlotWrapper.__getitem__ and .get
		self._pltattrs = dict()
		self._init_pltattrs()

		# To interface with the plot handler, there can be a loop return value
		self._loop_rv = None

	def _init_plot_func(self, modstr: str=None):
		'''Initialises the plot func by looking for the plot function name in the specified module or the default module PLOT_FUNC_MODSTR.'''
		if not modstr:
			modstr = self.PLOT_FUNC_MODSTR

		pf_name = self.plot_func_name

		# Import the module
		log.debug("Loading module '%s' ...", modstr)
		try:
			mod	= __import__(modstr, fromlist=["plt_" + pf_name])
		except ImportError:
			log.error("Failed to import plot func with name '%s'.", pf_name)
			raise

		# Get the plot function
		plot_func = getattr(mod, "plt_" + pf_name)

		# Done. Save to attribute.
		log.info("Got plotting function '%s'.", pf_name)
		self._plot_func = plot_func
		return

	def _init_style(self, update_with: dict=None):
		'''Initialises the _style attribute.'''

		log.debug("Initialising style ...")

		# Start with the base style
		sty	= copy.deepcopy(self.BASE_STYLE)

		# If given, update it from the class constant
		if self.UPDATE_BASE_STYLE:
			log.debugv("Recursively updating class constant with UPDATE_BASE_STYLE ...")
			sty = deval.tools.rec_update(sty, self.UPDATE_BASE_STYLE)

		# Load the plot_func-specific style, which is a plot_func attribute
		func_style = getattr(self.plot_func, 'style', None)
		if not func_style:
			if func_style is None:
				log.caution("No plot-function specific style defined "
				            "for '%s'!", self.plot_func_name)
			else:
				log.debug("Empty plot-function specific style defined "
				          "for '%s'.", self.plot_func_name)
			func_style = {}

		# ...and update the style with that
		sty = deval.tools.rec_update(sty, func_style)

		# Now update it with the given style, if present
		if update_with:
			sty = deval.tools.rec_update(sty, update_with)

		log.debug("Finished initialising style.")
		log.debugv("Complete style attribute:\n%s", deval.PP(sty))

		# Done. Can set the attribute now.
		self._style = sty
		return

	def _init_pltattrs(self):
		'''Initialising some plot attributes. Called from __init__'''

		log.debug("Initialising plot attributes ...")

		# Save kwargs that will not be modified by plot_func and/or are not needed there. This is necessary to do because they will not be accepted as kwargs by the plotting methods. Also, it guards against accidentaly changing them ...
		self['sp_kwargs'] = self.style.pop('subplots_kwargs')
		self['save_kwargs'] = self.style.pop('save_kwargs')

		self['mpl_style'] = self.style.pop('mpl_style', 'default')
		self['rc_dict'] = self.style.pop('rc_dict', {})
		self['rc_file'] = self.style.pop('rc_from_file', None)

		if not self['rc_file']:
			self['rc_file'] = self.DEFAULT_RC_FILE

		elif not os.path.isabs(self['rc_file']):
			raise ValueError("Given RC file path ({}) needs to be absolute! "
			                 "Correct the `rc_from_file` argument or set it "
			                 "to None to use the default values."
			                 "".format(self['rc_file']))

		# To provide an interface for plotting functions to add handles and labels to the legend of a specific ax, the following dictionary collects `handles`, and `labels` to be available during creation of the legend. The axis is used as the key of the dictionary.
		self['legend_hls'] 	= dict()

	# .........................................................................
	# Item access

	def __getitem__(self, key):
		return self._pltattrs[key]

	def __setitem__(self, key, val):
		self._pltattrs[key]	= val

	def get(self, key, default=None):
		return self._pltattrs.get(key, default)

	# .........................................................................
	# Properties
	# TODO make sure it is clear which ones are used on which level, copied or not...

	@property
	def plot_name(self):
		return self._plot_name

	@property
	def classname(self):
		return self.__class__.__name__

	@property
	def handler(self):
		return self._handler

	@property
	def plot_func(self):
		return self._plot_func

	@property
	def plot_func_name(self):
		return self._plot_func_name

	@property
	def func_kwargs(self):
		return self._func_kwargs

	@property
	def style(self):
		return self._style

	@property
	def loop_rv(self) -> dict:
		'''Dictionary to interface with the plot handler looping functionality.'''
		return self._loop_rv

	# Shortcuts within plot attributes
	# For the current plot ...

	@property
	def this_style(self):
		'''The style to use within a single plot'''
		return self['this_style']

	@property
	def this_func_kwargs(self):
		# Return the func_kwargs for the current plot
		return self['this_func_kwargs']

	# Figure related ...

	@property
	def fig(self):
		return self.get('fig')

	@fig.setter
	def fig(self, val):
		self['fig'] 	= val

	@property
	def axarr(self):
		return self.get('axarr')

	@axarr.setter
	def axarr(self, val):
		self['axarr'] 	= val

	@property
	def ax(self):
		'''Returns the current axis'''
		return plt.gca()

	@ax.setter
	def ax(self, ax):
		'''Sets the current axis object.'''
		return plt.sca(ax)

	# .........................................................................
	# The workflow of plotting

	def prepare_plotting(self):
		'''Prepares all necessary data for the plotting, given the plot configuration. This is the first method called and it is only called once; for methods called after an individual plot function was called, use prepare_single_plot'''

		log.note("Preparing plotting ...")

		# Stylesheet stuff . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Select the matplotlib plot stylesheet
		if self['mpl_style'] not in ['default']:
			log.state("Using stylesheet:  '%s'", self['mpl_style'])
			plt.style.use(self['mpl_style'])

		# Load additional RC parameters
		if self['rc_file']:

			log.debug("Loading contents of rc_file to use for update ...")
			rc_dict_base = deval.load_cfg_file(self['rc_file'])

			log.debug("Recursively updating the loaded rc parameters from the config file with the passed `rc_dict` values.")
			rc_dict	= deval.tools.rec_update(rc_dict_base, self['rc_dict'])

		# ...and, if defined, enter an RC context
		if self['rc_dict']:
			log.state("Using custom RC context.")
			log.debugv("rc_dict:\n  %s", self['rc_dict'])

			# Create the context object
			self['rc_context'] = mpl.rc_context(rc=self['rc_dict'])

			# ... and enter it.
			log.debug("Entering RC context ...")
			self['rc_context'].__enter__()

		# Prepare colormap and normalisation
		self.setup_cmap(**self.style.get('cmap_kwargs', {}))
		self.setup_cnorm(**self.style.get('cnorm_kwargs', {}))

	def prepare_single_plot(self, *, old_rv: dict=None):
		'''Called first thing when a single plot is performed.'''

		# Conservative approach: always work on copies or distached elements of the dicts, because someone could change these in plot_func and therefore mess things up ...
		self['this_func_kwargs'] 	= copy.deepcopy(self.func_kwargs)
		self['this_style']  		= copy.deepcopy(self.style)
		self['old_rv'] 				= old_rv
		self['add_artists'] 		= []

	def create_figure(self) -> tuple:
		'''Creates the figure and saves it to attributes.'''

		sp_kwargs = copy.deepcopy(self.get('sp_kwargs', {}))
		sp_shape = self._determine_subplot_shape()

		if sp_kwargs.pop('expand_to_sp_shape', False):
			sp_kwargs['figsize'][0]	*= sp_shape[0]
			sp_kwargs['figsize'][1]	*= sp_shape[1]

		log.debug("Creating figure ... (ncols=%d, nrows=%d)", *sp_shape)

		# Start the figure
		self.fig, self.axarr = plt.subplots(ncols=sp_shape[0],
		                                    nrows=sp_shape[1],
		                                    squeeze=False,
		                                    **sp_kwargs)

		# Transpose the axarr so that it can be accessed via (x, y) tuple
		self.axarr = self.axarr.T

		return self.fig, self.axarr

	def perform_plot(self) -> dict:
		'''Performs the plot by calling the plot function.'''
		self._call_plot_func()

	def postprocess_single_plot(self):
		'''Called after the single plot was finished'''
		log.note("Postprocessing single plot ...")

		# See if a title was set in style; if yes, perform a format operation on it and pass the func_kwargs
		if self.this_style.get('title'):
			log.debug("Performing format operation on title.")
			title_fstr = self.this_style['title']
			self.this_style['title'] = title_fstr.format(**self.func_kwargs)

		# Call the helpers
		self.call_helpers(*self.this_style['helpers'])

	def save_figure(self, *, fpath_fstr: str):
		'''Save the figure'''
		# Get the save kwargs
		sk = self.get('save_kwargs')

		# Save the figure
		log.note("Saving plot ...")
		fpath = fpath_fstr.format(ext=sk.get('save_figure_as', 'pdf'))
		plt.savefig(fpath,
		            additional_artists=tuple(self['add_artists']),
		            **sk.get('savefig_kwargs', {}))

		log.note("Saved to output directory as: '%s'", os.path.basename(fpath))
		log.state("Full path:\n  %s", fpath)

		# If configured, pickle the figure . . . . . . . . . . . . . . . .
		if sk.get('pickle_fig', False):
			log.note("Pickling figure object ...")

			# Generate filename and filepath
			pkl_fpath = fpath_fstr.format(ext='pkl')

			# Make sure that all needed directories exist
			log.debugv("Creating directories ...")
			os.makedirs(os.path.dirname(pkl_fpath), exist_ok=True)

			with open(pkl_fpath, 'wb') as pkl_file:
				pkl.dump(self.fig, pkl_file, **pk.get('pickle_kwargs'))

			log.debug("Pickled figure.")

	def close_figure(self):
		'''Closes the current figure.'''
		plt.close(self.fig)

	def postprocess_plotting(self):
		'''Used to clean up after all plots are done. This is the last method called and it is only called once, not after the plot_func is called!'''

		# Leave the rc context, if set
		if self.get('rc_context'):
			self['rc_context'].__exit__(None, None, None)
			log.debug("Left RC context.")

	# .........................................................................
	# Other public methods

	def register_legend_handle(self, *, ax, handle, label=None):
		'''Adds a single handle and the corresponding label to an attribute; this is read during creation of the legend.'''
		log.debug("Registering legend handle ...")
		log.debugv("Handle: %s", handle)
		log.debugv("Label:  %s", label)

		# Use the provided ax as the key and create a dictionary if needed.
		if ax not in self['legend_hls']:
			self['legend_hls'][ax] = dict(handles=[handle], labels=[label])
			log.debugv("Created legend register for this axis.")
		else:
			self['legend_hls'][ax]['handles'].append(handle)
			self['legend_hls'][ax]['labels'].append(label)
			log.debugv("Appended to legend register of this axis.")

		# Done.
		return

	def register_additional_artists(self, *artists):
		'''Registers the additional artists such that they can be passed to the savefig method and be considered in layouting.'''
		self['add_artists'] += artists
		log.debug("Registered %d additional artist(s).", len(artists))

	# .........................................................................
	# General helper methods

	def _determine_subplot_shape(self) -> tuple:
		'''Returns (num_rows, num_cols) tuple.'''
		raise NotImplementedError("needs to be subclassed")

	def call_helpers(self, *helpers):
		'''Method that calls the helpers with the names from the provided list.'''
		# Calling helpers
		if len(helpers) > 0:
			log.note("Calling %d helpers ...\n  %s", len(helpers),
		             ", ".join(helpers))

		for hlp_name in helpers:
			log.progress("Calling '%s' ...", hlp_name)
			# Get the function
			hlp_func	= getattr(self, '_hlpr_' + hlp_name)

			# Get corresponding arguments
			kwargs 		= self.this_style.get(hlp_name, {})
			log.debugv("kwargs: %s", deval.PP(kwargs))

			# Call it with those kwargs
			try:
				hlp_func(**kwargs)
			except Exception as err:
				log.error("There was an Exception raised during a '%s' helper call.\n\n%s: %s", hlp_name, err.__class__.__name__, err)
				if deval.DEBUG:
					raise
				else:
					log.caution("Continuing to plot ...")
			else:
				log.debug("Helper '%s' finished.", hlp_name)
		else:
			log.debug("%d helpers finished.", len(helpers))

	def _call_plot_func(self):
		log.progress("Calling plot function '%s' ...", self.plot_func_name)
		# Call the plot function
		try:
			self.plot_func(self,
		                   self.handler.dm,
		                   fig=self.fig, ax=self.ax,
		                   name=self.plot_name,
		                   style=self.this_style,
		                   cmap=self['cmap'], # TODO too prominent; find other way
		                   **self.this_func_kwargs)
		except Exception as err:
			log.error("%s raised during call to plot function '%s'.\n\n%s: %s", err.__class__.__name__, self.plot_func_name, err.__class__.__name__, err)
			if deval.DEBUG:
				raise
			else:
				log.caution("Continuing to plot ...")
		else:
			log.progress("Plot function '%s' finished.", self.plot_func_name)

		return

	def setup_cmap(self, *, name: str=None, bad: str='r', under: str='k', over='w'):
		'''Sets up a colormap with its bad, under, and over values'''
		cmap 	= plt.get_cmap(name=name)

		cmap.set_bad(bad)
		cmap.set_under(under)
		cmap.set_over(over)

		self['cmap'] 	= cmap

		return cmap

	def setup_cnorm(self, **cnorm_kwargs):
		''' Get the normalisation method to compute colormap values.'''

		# Save to attribute
		self['cnorm'] 	= get_cnorm(cmap=self['cmap'], **cnorm_kwargs)

		log.debug("Normalisation function saved to attribute: %s",
		          self['cnorm'])

		return self['cnorm']

	# .........................................................................
	# "Helper" methods, all prefixed with _hlpr_
	# Log levels should be DEBUG or DEBUGV (except CAUTION, WARNING)

	def _hlpr_set_limits(self, *, xlims: Union[str, Tuple[float, float]]=None, ylims: Union[str, Tuple[float, float]]=None, xflip: bool=False, yflip: bool=False):
		'''Sets the axis limits of the plot.

		Arguments `xlims` and `ylims` can have the following shapes:
			None, 'auto' 	-> leave setting the limits to matplotlib
			(0., 10.) 		-> use exactly this interval
			(0., None) 		-> Set only the left limit
			(None, 10.)		-> Set only the right limit
			'minmax' 		-> Set left to min(data), right to max(data)
			'zero' 			-> Set left/bottom to zero, the other to auto

		Arguments `xflip` and `yflip` invert the corresponding axes, such that (lower, upper) becomes (upper, lower).
		'''

		def parse_limit_arg(arg: Union[str, Tuple[float, float]], *, orientation: str, ax, flip: bool=False) -> dict:
			# Prepare a list in which to store the two limit values
			lims 		= [None, None]

			# Distinguish different cases
			if arg is None or arg == 'auto':
				# Nothing to do
				pass

			elif arg in ['minmax']:
				# Assume nans to allow comparison
				lims 	= [np.nan, np.nan]

				# Need to get the data from the lines in the plot
				lines 	= ax.get_lines()

				log.debugv("Searching min and max values from %d lines...", len(lines))

				for line in lines:
					if orientation == "x":
						line_data 	= line.get_xdata()
					elif orientation == "y":
						line_data 	= line.get_ydata()
					else:
						raise ValueError("invalid orientation: " + orientation)

					# Keep min and max values updated
					lims[0] = np.nanmin([lims[0], np.nanmin(line_data)])
					lims[1] = np.nanmax([lims[1], np.nanmax(line_data)])

				# Convert nans back to Nones
				lims 	= [None if l is np.nan else l for l in lims]

			elif arg in ['zero', 'zeroleft', 'zerobottom']:
				# Set the left/lower border to zero
				lims[0] = 0.

			elif isinstance(arg, str):
				# No further string arguments allowed. Warn.
				log.warning("No valid limit argument given for %s-axis. Expected string (%s) or sequence of length 2, got: %s\nUsing automatic limits.", orientation, ", ".join(['auto', 'minmax', 'zero']), arg)

			else:
				# Now the argument is expected to be a sequence of length 2
				try:
					lims[0], lims[1] 	= arg

				except ValueError as err:
					log.warning("Invalid limit argument for %s-axis. Needs to be a sequence of length 2, was: %s", orientation, arg)

			# If flip flag is set, invert the limits tuple
			if flip:
				lims 	= (lims[1], lims[0])

			# Create and return the return dictionary
			if orientation == "x":
				return dict(left=lims[0], right=lims[1])

			elif orientation == "y":
				return dict(bottom=lims[0], top=lims[1])

			else:
				raise ValueError("invalid orientation: " + orientation)

		# Get the axis
		ax 	= self.ax

		# Now apply these limits
		if xlims:
			xlims 	= parse_limit_arg(xlims, orientation="x",
			                          ax=ax, flip=xflip)

			log.debug("Setting xlims to:  %s", xlims)
			ax.set_xlim(**xlims)

		if ylims:
			ylims 	= parse_limit_arg(ylims, orientation="y",
			                          ax=ax, flip=yflip)

			log.debug("Setting ylims to:  %s", ylims)
			ax.set_ylim(**ylims)

	def _hlpr_set_labels(self, *, xlabel: str=None, ylabel: str=None, **label_kwargs):
		'''Sets the labels of the given figure.'''
		if xlabel:
			plt.xlabel(xlabel, **label_kwargs)

		if ylabel:
			plt.ylabel(ylabel, **label_kwargs)

	def _hlpr_set_label_coords(self, *, xlabel_coords: tuple=None, ylabel_coords: tuple=None):
		'''Sets the label coordinates.'''
		if xlabel_coords:
			# First entry (for x-position) can be left out -> center it
			if not isinstance(xlabel_coords[0], (int, float)):
				xlabel_coords 	= [0.5, xlabel_coords[1]]

			log.debug("Setting xlabel coordinates: %s", xlabel_coords)
			self.ax.xaxis.set_label_coords(*xlabel_coords)

		if ylabel_coords:
			# Second entry (for y-position) can be left out -> center it
			if not isinstance(ylabel_coords[1], (int, float)):
				ylabel_coords 	= [ylabel_coords[0], 0.5]

			log.debug("Setting xlabel coordinates: %s", xlabel_coords)
			self.ax.yaxis.set_label_coords(*ylabel_coords)

	def _hlpr_set_title(self, *, title: str=None, as_suptitle: bool=False, **title_kwargs):
		'''Sets the title of the current figure.'''
		if not title:
			log.debugv("Getting title from current style ...")
			title 	= self.this_style.get('title')

		if title:
			if as_suptitle:
				log.debug("Setting suptitle to:  %s", title)
				plt.suptitle(title, **title_kwargs)
			else:
				log.debug("Setting title to:  %s", title)
				plt.title(title, **title_kwargs)

	def _hlpr_set_suptitle(self, *, title: str=None, **title_kwargs):
		'''Sets the suptitle of the current figure.'''
		self._hlpr_set_title(title=title, as_suptitle=True, **title_kwargs)

	def _hlpr_set_legend(self, *, add_from_registry: bool=True, hide: bool=False, unique_labels: bool=False, zorder: int=100, bbox_relative_to: str=None, as_figlegend: bool=False, title_only: bool=False, multialignment: str=None, **legend_kwargs):
		'''Sets the legend of the current axis'''

		# Get the axis
		ax 	= self.ax

		# Might want to actively hide the legend
		if hide:
			log.debugv("Hiding legend ...")
			ax.legend().set_visible(False)
			return

		# Get the handles and labels for this axis
		if not title_only:
			h, l 	= ax.get_legend_handles_labels()
			log.debugv("Current handles and labels:\n\n%s\n\n%s", h, l)

			# Add those from the legend registry
			if add_from_registry:
				log.debugv("Adding from registry ...")
				_reg 	= self['legend_hls'].get(ax)

				log.debugv("Legend registry of handles and labels:\n%s", _reg)

				if _reg:
					h 		+= _reg['handles']
					l 		+= _reg['labels']
				else:
					log.debugv("No entries in legend registry for the selected axis.")

			# Only show unique labels
			if unique_labels:
				log.debugv("Making labels unique ...")
				l, ids 	= np.unique(l, return_index=True)
				h 		= [h[i] for i in ids]

			# There might be a bbox given; set it relative to the axis or to the figure depending on which argument is passed
			if bbox_relative_to in ['axis', 'axes', 'ax']:
				# this is the default; need not add bbox_transform
				pass
			elif bbox_relative_to in ['fig', 'figure']:
				legend_kwargs['bbox_transform'] = self.fig.transFigure

			# Catch the case where no handles are defined
			if not h:
				log.caution("No handles available for legend.")
				try:
					ax.legend().set_visible(False)
				except Exception as err:
					log.debug("Failed making legend invisible, probably because there was no legend on this axis.\n%s: %s", err.__class__.__name__, err)
				return

		else:
			log.debugv("Setting only the legend title ...")
			# To only add the title, set empty handles and labels lists
			h, l = [], []

			# Warn if there was no title
			if not legend_kwargs.get('title'):
				log.caution("Setting legend with `only_title==True`, but there was no title.")

		# Now add the legend
		log.debugv("Setting legend with handles and labels:\n\n%s\n\n%s", h, l)
		log.debugv("legend_kwargs:\n%s", legend_kwargs)

		if not as_figlegend:
			lgd = plt.legend(h, l, **legend_kwargs)

		else:
			log.debugv("Setting as figure legend ...")
			raise NotImplementedError('as_figlegend')
			# lgd = self.fig.legend(h, l, **legend_kwargs)

		# Now having the legend object, set its zorder and multialignment argument
		lgd.set_zorder(zorder)

		if multialignment:
			plt.setp(lgd.get_title(), multialignment=multialignment)

		# Done
		return lgd

	def _hlpr_set_figlegend(self, **legend_kwargs):
		'''Calls the legend helper with the figlegend flag set.'''
		raise NotImplementedError('set_figlegend')
		# TODO might need to do stuff here, getting handles etc.
		self._hlpr_set_legend(as_figlegend=True, **legend_kwargs)

	def _hlpr_add_cbar(self, *, cbar_rect, cnorm_kwargs=None, label: str=None, num_ticks: int=None, tick_locator_kwargs: dict=None, **cbar_kwargs):
		'''Adds a colorbar to the given figure.'''

		# First, need a colormap to display.
		cmap 	= self['cmap']

		# Then, need a normalisation function
		if cnorm_kwargs:
			# Create a new normalisation function from the given interval
			cnorm	= get_cnorm(cmap=cmap, **cnorm_kwargs)

		elif self['cnorm'] is not None:
			# Use the pre-created one
			cnorm 	= self['cnorm']

		else:
			log.warning("Need a normalisation function for adding a colorbar, but there was neither an explicit data interval given nor a cnorm initialised in the wrapper. Shown colorbar will not have the correct normalisation!")
			cnorm 	= None
			label 	= "{} - (incorrect normalisation!)".format(label if label
			                                                   else "")

		# Add the axis, but don't make it the current one
		ca 		= self.ax
		cb_ax 	= self.fig.add_axes(cbar_rect)
		self.ax = ca

		# Create the colorbar
		cbar 	= mpl.colorbar.ColorbarBase(cb_ax, cmap=cmap, norm=cnorm,
		                                    **cbar_kwargs)

		# Set the number of ticks
		if num_ticks is not None:
			cbar.locator = mpl.ticker.MaxNLocator(nbins=num_ticks,
			                                      **(tick_locator_kwargs
			                                         if tick_locator_kwargs
			                                         else {})
			                                      )
			cbar.update_ticks()

		# Set the label, if given
		if label:
			cbar.set_label(str(label))

		# Register the artist
		self.register_additional_artists(cbar)

		# Done
		return

	def _hlpr_set_hatched_background(self, range_x=None, range_y=None, **hatch_kwargs):
		'''Sets a hatched rectangle into the background of the current axis.'''
		xrg 	= range_x if range_x else self.ax.get_xlim()
		yrg 	= range_y if range_y else self.ax.get_ylim()

		_xy 		= (xrg[0], yrg[0])
		_w, _h 		= xrg[1] - xrg[0], yrg[1] - yrg[0]
		self.ax.add_patch(Rectangle(_xy, _w, _h, zorder=-10, **hatch_kwargs))

		return

	def _hlpr_set_scales(self, xscale: str=None, yscale: str=None, xscale_kwargs: dict=None, yscale_kwargs: dict=None):
		'''Sets the x and y scales of the current axis'''
		log.debug("Setting scales:\n  x: %s %s\n  y: %s %s", xscale, xscale_kwargs, yscale, yscale_kwargs)

		if xscale:
			if xscale not in set_cat_scale.cat_scales:
				self.ax.set_xscale(xscale,
			    	               **(xscale_kwargs if xscale_kwargs else {}))
			else:
				if not xscale_kwargs:
					raise ValueError("Need argument `xscale_kwargs` for setting categorical scale.")
				set_cat_scale(self, ax_obj=self.ax.xaxis,
				              mode=xscale, **xscale_kwargs)

		if yscale:
			if yscale not in set_cat_scale.cat_scales:
				self.ax.set_yscale(yscale,
				                   **(yscale_kwargs if yscale_kwargs else {}))
			else:
				if not yscale_kwargs:
					raise ValueError("Need argument `yscale_kwargs` for setting categorical scale.")
				set_cat_scale(self, ax_obj=self.ax.yaxis,
				              mode=yscale, **yscale_kwargs)

	def _hlpr_set_aspect(self, **aspect_kwargs):
		'''Sets the aspect ratio of the current axis'''
		log.debugv("Setting the aspect of the current axis using the following arguments:  %s", aspect_kwargs)
		self.ax.set_aspect(**aspect_kwargs)

	def _hlpr_set_tick_locators(self, *, x: dict=None, y: dict=None):
		'''Sets the tick locator.

		Expects for `x` and/or `y` a dict with the keys `major` and `minor`, in it a key with the `name` of the locator, and all the rest of the keys are considered to be kwargs to that locator.'''

		def set_tick_locator(ax, *, major: bool, name: str, init_kwargs: dict=None, **tl_params):
			TLcls 	= getattr(mpl.ticker, name)
			tl 		= TLcls(**(init_kwargs if init_kwargs else {}))

			# Read the tl_params, that are regarded as key, kwarg pairs, where the key has to match a callable attribute of TLcls
			for attr_name, params in tl_params.items():
				args 	= params.get('args', [])
				kwargs 	= params.get('kwargs', {})
				getattr(tl, attr_name)(*args, **kwargs)

			if major:
				ax.set_major_locator(tl)
			else:
				ax.set_minor_locator(tl)
			log.debugv("Successfully set %s (major: %s).", TLcls, major)
			return

		if x:
			log.debug("Setting x tick locators...")
			log.debugv("kwargs:\n%s", x)

			x_ax 	= self.ax.xaxis

			if x.get('major'):
				set_tick_locator(x_ax, major=True, **x['major'])
			if x.get('minor'):
				set_tick_locator(x_ax, major=False, **x['minor'])

		if y:
			log.debug("Setting y tick locators...")
			log.debugv("kwargs:\n%s", y)

			y_ax 	= self.ax.yaxis

			if y.get('major'):
				set_tick_locator(y_ax, major=True, **y['major'])
			if y.get('minor'):
				set_tick_locator(y_ax, major=False, **y['minor'])
		return

	def _hlpr_set_tick_formatters(self, *, x: dict=None, y: dict=None):
		'''Sets the tick formatters.

		Expects for `x` and/or `y` a dict with the keys `major` and `minor`, in it a key with the `name` of the formatter, and all the rest of the keys are considered to be kwargs to that formatter.'''

		def set_tick_formatter(ax, *, major: bool, name: str, init_kwargs: dict=None, func_name: str=None, **tf_params):
			if name != "FuncFormatter":
				TFcls 	= getattr(mpl.ticker, name)
				tf 		= TFcls(**(init_kwargs if init_kwargs else {}))

				# Read the tf_params, that are regarded as key, kwarg pairs, where the key has to match a callable attribute of TFcls
				for attr_name, params in tf_params.items():
					args 	= params.get('args', [])
					kwargs 	= params.get('kwargs', {})
					getattr(tf, attr_name)(*args, **kwargs)

			elif func_name:
				# Still get the TickFormatter class
				TFcls 		= getattr(mpl.ticker, name)

				# Get another helper function to do this
				func		= getattr(self, "_tick_formatter_"+func_name)

				# Make it possible to pass arguments by creating a partial function call
				func		= partial(func, **tf_params)

				# Create the FuncFormatter
				tf 			= TFcls(func)

			else:
				raise ValueError("Chose FuncFormatter but did not supply `func_name`.")

			if major:
				ax.set_major_formatter(tf)
			else:
				ax.set_minor_formatter(tf)
			log.debugv("Successfully set %s (major: %s).", TFcls, major)
			return

		if x:
			log.debug("Setting x tick formatters...")
			log.debugv("kwargs:\n%s", x)

			x_ax 	= self.ax.xaxis

			if x.get('major'):
				set_tick_formatter(x_ax, major=True, **x['major'])
			if x.get('minor'):
				set_tick_formatter(x_ax, major=False, **x['minor'])

		if y:
			log.debug("Setting y tick formatters...")
			log.debugv("kwargs:\n%s", y)

			y_ax 	= self.ax.yaxis

			if y.get('major'):
				set_tick_formatter(y_ax, major=True, **y['major'])
			if y.get('minor'):
				set_tick_formatter(y_ax, major=False, **y['minor'])
		return

	def _hlpr_set_hv_lines(self, *, hlines: Sequence[dict]=None, vlines: Sequence[dict]=None, **base_style):
		'''Adds horizontal and vertical lines to the plot'''
		if hlines:
			log.debug("Adding hlines:\n%s", vlines)
			for hline in hlines:
				_kwargs	= deval.tools.rec_update(copy.deepcopy(base_style),
				                                 hline)
				plt.axhline(**_kwargs)

		if vlines:
			log.debug("Adding vlines:\n%s", vlines)
			for vline in vlines:
				_kwargs	= deval.tools.rec_update(copy.deepcopy(base_style),
				                                 vline)
				plt.axvline(**_kwargs)

	def _hlpr_add_annotations(self, *, annotations: list=None):
		'''Adds the annotations specified in the list of annotations.'''
		if not annotations:
			return

		for ann in annotations:
			# Work on a copy
			ann 		= copy.deepcopy(ann)

			# Resolve transformations
			tnames 		= []
			xtrans 		= ann.pop('xtrans', None)
			ytrans 		= ann.pop('ytrans', None)
			if xtrans:
				tnames.append(xtrans)
			if ytrans:
				tnames.append(ytrans)

			if tnames:
				_args 	= [getattr(self.ax, tname) for tname in tnames]
				trans 	= transforms.blended_transform_factory(*_args)

			# Perform the annotation
			self.ax.annotate(ann.pop('text'), xycoords=trans, **ann)
		else:
			log.debug("Added %d annotations.", len(annotations))

	def _hlpr_subplots_adjust(self, wspace=None, hspace=None, scale_with_wh_ratio: bool=True, **kwargs):
		if scale_with_wh_ratio and wspace and hspace:
			# As wspace and hspace are relative values, the gap between subplots depends on the figure size. Scale wspace with aspect ratio of figure to make the gaps equally wide if wspace == hspace
			wspace 	*= self.fig.get_figheight()/self.fig.get_figwidth()
		self.fig.subplots_adjust(wspace=wspace, hspace=hspace, **kwargs)

	def _hlpr_tight_layout(self, **kwargs):
		'''Calls plt.tight_layout with the given kwargs.'''
		plt.tight_layout(**kwargs)

	# .........................................................................
	# Tick formatters

	def _tick_formatter_multiply_with(self, x, pos, *, factor: float=None, fstr: str=None) -> str:
		'''Given the tick value `x` and the position `pos`, returns a new tick label.'''
		if factor is not None:
			x 	*= factor

		if fstr:
			x 	= fstr.format(x)

		return x


# -----------------------------------------------------------------------------

class SinglePlot(PlotWrapper):
	'''Plot wrapper that specialises on a single plot.'''

	def _determine_subplot_shape(self) -> tuple:
		'''Always (1, 1)'''
		self._sp_shape 	= (1, 1)
		return (1, 1)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# helpers

def get_cnorm(*, intv: tuple=None, scale: str='linear', linthresh: float=None, linscale: float=1.0, clip: bool=False, cmap=None,  quantized=None): # TODO categorial scale support!
	''' Get the normalisation method to compute colormap values.'''
	# Define lists to more easily check for valid scale values
	LIN_SCALES	= ['lin', 'linear']
	LOG_SCALES	= ['log', 'symlog']

	# only create the entry, if an interval was given. Pretty meaning less otherwise.
	if intv is None:
		log.debug("No interval given -> not setting up normalisation function.")
		return

	log.debug("Setting up normalisation function ...")

	# Get variables
	norm 		= None

	log.debugv("intv: %s,  scale: %s", intv, scale)

	if quantized:
		# Compute for quantised normalisation
		bounds 	= None
		if isinstance(quantized, (tuple, list)):
			bounds 	= list(quantized)
		elif isinstance(quantized, int):
			# was boolean, use the same interval but quantize
			if scale in LIN_SCALES:
				bounds 	= list(np.linspace(intv[0], intv[1], num=quantized))

			elif scale in LOG_SCALES:
				if scale == 'symlog':
					raise NotImplementedError("Quantized symlog scale not implemented.")

				# use logspace
				_args 	= (np.log(intv[0])/np.log(10),
				          np.log(intv[1])/np.log(10))
				bounds 	= list(np.logspace(*_args, num=quantized))

		else:
			log.error("Invalid value '%s' (%s) for argument 'quantized'; using non-quantized scale instead.",
			            quantized, type(quantized))

		if bounds:
			# Set the quantised norm function; needs the number of quantisation steps from the colormap
			if cmap is None:
				log.error("No `cmap` argument passed but needed for BoundaryNorm!")
			else:
				norm	= mpl.colors.BoundaryNorm(bounds, cmap.N)

	if not norm:
		# Set smooth normalisation
		if scale is None:
			norm	= mpl.colors.Normalize(clip=clip)
		elif scale in LIN_SCALES:
			norm	= mpl.colors.Normalize(vmin=intv[0], vmax=intv[1],
			                               clip=clip)
		elif scale in LOG_SCALES:
			if scale == 'log':
				norm	= mpl.colors.LogNorm(vmin=intv[0], vmax=intv[1],
			    	                         clip=clip)
			elif scale == 'symlog':
				if not linthresh:
					linthresh 	= min(1e-4, intv[1])
					log.caution("symlog argument linthresh not given, chose automatically: %f", linthresh)

				norm = mpl.colors.SymLogNorm(linthresh=linthresh,
				                             vmin=intv[0], vmax=intv[1],
				                             linscale=linscale, clip=clip)

		else:
			log.error("Invalid normalisation scale %s!", scale)

	return norm

def set_cat_scale(wrpr, *, ax_obj, mode: str, labels: Sequence[str], locations: Sequence[float]=None):
	'''Helper for setting categorical scales'''

	# Distinguish scale modes and determine locator positions
	if mode in ['cat', 'categorical', 'index']:
		# Create list of indices
		locations	= [i for i, _ in enumerate(labels)]

		log.debug("Setting categorical scale with labels: %s", labels)

	elif mode in ['at_value', 'cat_at_value']:
		if locations is None:
			raise ValueError("Need to supply `locations` argument to use categorical scale at set locations!")
		# Can use these locations
		log.debug("Setting categorical scale with\n  labels:   %s\n  locations: %s", labels, locations)

	else:
		raise ValueError("Invalid categorical scale argument `mode`: "+str(mode))

	# Create the locator
	locator 	= mpl.ticker.FixedLocator(locations)

	# Set the locator to the ax obj; unset any minor locator
	ax_obj.set_major_locator(locator)
	ax_obj.set_minor_locator(mpl.ticker.NullLocator())

	# Set the formatter with use of the labels
	formatter 	= mpl.ticker.FixedFormatter(labels)
	ax_obj.set_major_formatter(formatter)

	return
set_cat_scale.cat_scales = ['cat', 'categorical', 'index', 'at_value', 'cat_at_value']
