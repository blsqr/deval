#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''A collection of general tools.

Everything in here should stand on its own and not depend on anything else in the deval package.

NOTE: this file is made available via the deval.__init__ module, and can thus not depend on any other modules. Furthermore, it does not have access to a config file.
'''

import sys
import subprocess
import collections
import logging
from pprint import PrettyPrinter
import json

import numpy as np

# Setup logging for this file
log	= logging.getLogger(__name__)
log.debug("Loaded logger.")

# Local constants
# Terminal, TTY-related
IS_A_TTY 	= sys.stdout.isatty()
try:
	_, TTY_COLS	= subprocess.check_output(['stty', 'size']).split()
except:
	# Probably not run from terminal --> set value manually
	TTY_COLS 	= 79
else:
	TTY_COLS 	= int(TTY_COLS)
log.debug("Determined TTY_COLS: %s, IS_A_TTY: %s", TTY_COLS, IS_A_TTY)

# -----------------------------------------------------------------------------
# dictionary and list related methods

def rec_update(d, u):
	''' Recursively updates the Mapping-like object d with the Mapping-like object u and returns the updated Mapping-like object.

	NOTE: This does not create a copy of `d`!

	From: http://stackoverflow.com/a/32357112/1827608
	'''
	for k, v in u.items():
		if isinstance(d, collections.Mapping):
			# Already a Mapping
			if isinstance(v, collections.Mapping):
				# Already a Mapping, continue recursion
				d[k] = rec_update(d.get(k, {}), v)
			else:
				# Not a mapping -> at leaf -> update value
				d[k] = v 	# ... which is just u[k]

		else:
			# Not a mapping -> create one
			d = {k: u[k]}
	return d

# -----------------------------------------------------------------------------
# Pretty printing
# TODO make this nicer ...

pprint 	= PrettyPrinter(indent=2, width=TTY_COLS, compact=False).pformat

class TreePrinter:
	'''Can be used to return a prettified string from data.'''

	def __init__(self, skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True, cls=None, indent=None, separators=None, default=None, sort_keys=False, **kw):

		self.json_params = dict(skipkeys=skipkeys, ensure_ascii=ensure_ascii, check_circular=check_circular, allow_nan=allow_nan, cls=cls, indent=indent, separators=separators, default=default, sort_keys=sort_keys, **kw)

	def __call__(self, obj, key_length: int=37, **tree_kwargs):
		'''Given an object, return a prettified string. If as_tree is True, it is tried to return the object in a tree-like structure.'''

		# Formatting keys and objects ...
		obj = format_tree(obj, key_length=key_length, **tree_kwargs)

		# Create serialised json string
		s = json.dumps(obj, **self.json_params)

		# Stripping json syntax
		s = s.replace("\n", "", 1) # first line break
		s = s.replace("\"", "")
		s = s.replace("{", "")
		s = s.replace("},", " /")
		s = s.replace("}", " /")
		s = s.replace(">,", ">")
		s = s.replace(":", "")
		s = s.replace(" ->", ":")

		# Remove first two spaces in each line (due to outer dict and json)
		s = "\n".join([line[2:] for line in s.split('\n')
		              if len(line) >= 2])

		return s

def format_tree(tree: dict, key_length: int=15, key_fstr: str=None, key_suffix: str=" ->", obj_fstr: str="<{}>"):
	'''
	If the obj_fstr argument is given, it is used to format the leafs of the tree, i.e. the non-DataGroup-derived objects.

	The key_suffix and key_fstr argument can be used to format the key of the resulting dictionary. This is used in the __str__ method to allow structured output.'''

	orig_tree 	= tree
	tree 		= {}

	if not key_fstr and key_length:
		key_fstr 	= "|- {:"+str(key_length+len(key_suffix))+"s}"

	for key, obj in orig_tree.items():

		# Need to work on strings here ...
		key 	= str(key)

		if key_suffix:
			key 	+= key_suffix

		# Format the key
		if key_fstr:
			key 	= key_fstr.format(key)

		if isinstance(obj, collections.Mapping):
			# Continue recursion
			tree[key] 	= format_tree(obj,
			                          key_length=key_length, key_fstr=key_fstr,
			                          key_suffix=key_suffix, obj_fstr=obj_fstr)

		else:
			# Reached leaf
			try:
				tree[key] 	= obj_fstr.format(obj=obj)

			except:
				log.debugv("Applying format string '%s' failed for object of type %s. Using str method instead.", obj_fstr, obj.__class__.__name__)
				# Just use the string
				tree[key] 	= str(obj)

	return tree

# -----------------------------------------------------------------------------
# Terminal formatting

def clear_line(only_in_tty=True, break_if_not_tty=True):
	''' Clears the current terminal line and resets the cursor to the first position using a POSIX command.'''
	# Based on: http://stackoverflow.com/questions/5419389/how-to-overwrite-the-previous-print-to-stdout-in-python

	if break_if_not_tty or only_in_tty:
		# check if there is a tty
		is_tty = sys.stdout.isatty()

	# Differentiate cases
	if (only_in_tty and is_tty) or not only_in_tty:
		# Print the POSIX character
		print('\x1b[2K\r', end='')

	if break_if_not_tty and not is_tty:
		# print linebreak (no flush)
		print('\n', end='')

	# no linebreak, flush manually
	sys.stdout.flush()

def fill_tty_line(s: str, fill_char: str=" ", align: str="left") -> str:
	'''If the terminal is a tty, returns a string that fills the whole tty line with the specified fill character.'''
	if not IS_A_TTY:
		return s

	fill_str = fill_char * (TTY_COLS - len(s))

	if align in ["left", "l", None]:
		return s + fill_str

	elif align in ["right", "r"]:
		return fill_str + s

	elif align in ["center", "centre", "c"]:
		return fill_str[:len(fill_str)//2] + s + fill_str[len(fill_str)//2:]

	else:
		raise ValueError("align argument '{}' not supported".format(align))

def tty_centred_msg(s: str, fill_char: str="·") -> str:
	'''Shortcut for a common fill_tty_line use case.'''
	return fill_tty_line(s, fill_char=fill_char, align='centre')


# -----------------------------------------------------------------------------
# Math

def std(a: np.array, weights: np.array=None, axis: int=None, **std_kwargs) -> np.array:
	'''Calculates the standard deviation of array `a`. If weights are given, uses these weights to do so.

	Without weights, np.nanstd is used. Kwargs are passed on.
	'''

	if weights is None:
		return np.nanstd(a, axis=axis, **std_kwargs)

	else:
		log.debugv("Using given weights to calculate std ...")

		if std_kwargs:
			log.caution("The following additional kwargs to the tools.std call will be ignored:  %s", std_kwargs)

		# Use the average to calculate the variance
		avg	= np.average(a, weights=weights, axis=axis)
		var	= np.average((a - avg)**2, weights=weights, axis=axis)

		# And now apply the square root
		return np.sqrt(var)

