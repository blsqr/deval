#!/usr/bin/env python3

from setuptools import setup

setup(name='deval',
      version='0.3.7',
      description='General data evaluation package.',
      long_description='A package meant for advanced data analysis and plotting; this can be extended to become more specific to the data that is desired to be analysed.',
      author='Yunus Sevinchan',
      author_email='Yunus.Sevinchan@iup.uni.heidelberg.de',
      url='https://ts-gitlab.iup.uni-heidelberg.de/yunus/deval',
      licence='MIT',
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.6',
          'Topic :: Utilities'
          ],
      packages=['deval'],
      package_data=dict(deval=["*.yml"]),
      install_requires=[
          'colorlog>=2.10.0',
          'dill>=0.2.7.1',
          'easydict>=1.7',
          'h5py>=2.7.0',
          'matplotlib>=2.2',
          'numpy>=1.14',
          'pandas>=0.21',
          'PyYAML>=3.12',
          'scipy>=1.0',
          'seaborn>=0.8'
          ],
     )
